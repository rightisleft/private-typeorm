"use strict";
/**
 * Thrown when consumer tries to release entity manager that does not use single database connection.
 */
class NoNeedToReleaseEntityManagerError extends Error {
    constructor() {
        super();
        this.name = "NoNeedToReleaseEntityManagerError";
        this.message = `Entity manager is not using single database connection and cannot be released. ` +
            `Only entity managers created by connection#createEntityManagerWithSingleDatabaseConnection ` +
            `methods have a single database connection and they should be released.`;
        this.stack = new Error().stack;
    }
}
exports.NoNeedToReleaseEntityManagerError = NoNeedToReleaseEntityManagerError;

//# sourceMappingURL=NoNeedToReleaseEntityManagerError.js.map
