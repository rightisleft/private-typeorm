"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const ConnectionIsNotSetError_1 = require("../error/ConnectionIsNotSetError");
const DriverPackageNotInstalledError_1 = require("../error/DriverPackageNotInstalledError");
const DriverPackageLoadError_1 = require("../error/DriverPackageLoadError");
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const ColumnMetadata_1 = require("../../metadata/ColumnMetadata");
const moment = require("moment");
const SqliteQueryRunner_1 = require("./SqliteQueryRunner");
const DriverOptionNotSetError_1 = require("../error/DriverOptionNotSetError");
/**
 * Organizes communication with sqlite DBMS.
 */
class SqliteDriver {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(connectionOptions, logger, sqlite) {
        this.options = connectionOptions;
        this.logger = logger;
        this.sqlite = sqlite;
        // validate options to make sure everything is set
        if (!this.options.storage)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("storage");
        // if sqlite package instance was not set explicitly then try to load it
        if (!sqlite)
            this.loadDependencies();
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Performs connection to the database.
     */
    connect() {
        return new Promise((ok, fail) => {
            const connection = new this.sqlite.Database(this.options.storage, (err) => {
                if (err)
                    return fail(err);
                this.databaseConnection = {
                    id: 1,
                    connection: connection,
                    isTransactionActive: false
                };
                ok();
            });
        });
    }
    /**
     * Closes connection with database.
     */
    disconnect() {
        return new Promise((ok, fail) => {
            const handler = (err) => err ? fail(err) : ok();
            if (!this.databaseConnection)
                return fail(new ConnectionIsNotSetError_1.ConnectionIsNotSetError("sqlite"));
            this.databaseConnection.connection.close(handler);
        });
    }
    /**
     * Creates a query runner used for common queries.
     */
    createQueryRunner() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.databaseConnection)
                return Promise.reject(new ConnectionIsNotSetError_1.ConnectionIsNotSetError("sqlite"));
            const databaseConnection = yield this.retrieveDatabaseConnection();
            return new SqliteQueryRunner_1.SqliteQueryRunner(databaseConnection, this, this.logger);
        });
    }
    /**
     * Access to the native implementation of the database.
     */
    nativeInterface() {
        return {
            driver: this.sqlite,
            connection: this.databaseConnection ? this.databaseConnection.connection : undefined
        };
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type and metadata.
     */
    preparePersistentValue(value, column) {
        switch (column.type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value === true ? 1 : 0;
            case ColumnTypes_1.ColumnTypes.DATE:
                return moment(value).format("YYYY-MM-DD");
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value).format("HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.DATETIME:
                return moment(value).format("YYYY-MM-DD HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.stringify(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value
                    .map(i => String(i))
                    .join(",");
        }
        return value;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type or metadata.
     */
    prepareHydratedValue(value, columnOrColumnType) {
        const type = columnOrColumnType instanceof ColumnMetadata_1.ColumnMetadata ? columnOrColumnType.type : columnOrColumnType;
        switch (type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value ? true : false;
            case ColumnTypes_1.ColumnTypes.DATE:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD").toDate();
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value, "HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.DATETIME:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.parse(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value.split(",");
        }
        return value;
    }
    /**
     * Replaces parameters in the given sql with special escaping character
     * and an array of parameter names to be passed to a query.
     */
    escapeQueryWithParameters(sql, parameters) {
        if (!parameters || !Object.keys(parameters).length)
            return [sql, []];
        const builtParameters = [];
        const keys = Object.keys(parameters).map(parameter => "(:" + parameter + "\\b)").join("|");
        sql = sql.replace(new RegExp(keys, "g"), (key) => {
            const value = parameters[key.substr(1)];
            if (value instanceof Array) {
                return value.map((v) => {
                    builtParameters.push(v);
                    return "$" + builtParameters.length;
                }).join(", ");
            }
            else {
                builtParameters.push(value);
            }
            return "$" + builtParameters.length;
        }); // todo: make replace only in value statements, otherwise problems
        return [sql, builtParameters];
    }
    /**
     * Escapes a column name.
     */
    escapeColumnName(columnName) {
        return "\"" + columnName + "\"";
    }
    /**
     * Escapes an alias.
     */
    escapeAliasName(aliasName) {
        return "\"" + aliasName + "\"";
    }
    /**
     * Escapes a table name.
     */
    escapeTableName(tableName) {
        return "\"" + tableName + "\"";
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Retrieves a new database connection.
     * If pooling is enabled then connection from the pool will be retrieved.
     * Otherwise active connection will be returned.
     */
    retrieveDatabaseConnection() {
        if (this.databaseConnection)
            return Promise.resolve(this.databaseConnection);
        throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("sqlite");
    }
    /**
     * If driver dependency is not given explicitly, then try to load it via "require".
     */
    loadDependencies() {
        if (!require)
            throw new DriverPackageLoadError_1.DriverPackageLoadError();
        try {
            this.sqlite = require("sqlite3").verbose();
        }
        catch (e) {
            throw new DriverPackageNotInstalledError_1.DriverPackageNotInstalledError("SQLite", "sqlite3");
        }
    }
}
exports.SqliteDriver = SqliteDriver;

//# sourceMappingURL=SqliteDriver.js.map
