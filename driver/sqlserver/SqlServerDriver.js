"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const ConnectionIsNotSetError_1 = require("../error/ConnectionIsNotSetError");
const DriverPackageNotInstalledError_1 = require("../error/DriverPackageNotInstalledError");
const DriverPackageLoadError_1 = require("../error/DriverPackageLoadError");
const DriverUtils_1 = require("../DriverUtils");
const SqlServerQueryRunner_1 = require("./SqlServerQueryRunner");
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const moment = require("moment");
const ColumnMetadata_1 = require("../../metadata/ColumnMetadata");
const DriverOptionNotSetError_1 = require("../error/DriverOptionNotSetError");
/**
 * Organizes communication with SQL Server DBMS.
 */
class SqlServerDriver {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(options, logger, mssql) {
        /**
         * Pool of database connections.
         */
        this.databaseConnectionPool = [];
        this.options = DriverUtils_1.DriverUtils.buildDriverOptions(options);
        this.logger = logger;
        this.mssql = mssql;
        // validate options to make sure everything is set
        if (!this.options.host)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("host");
        if (!this.options.username)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("username");
        if (!this.options.database)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("database");
        // if mssql package instance was not set explicitly then try to load it
        if (!mssql)
            this.loadDependencies();
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Performs connection to the database.
     * Based on pooling options, it can either create connection immediately,
     * either create a pool and create connection when needed.
     */
    connect() {
        // build connection options for the driver
        const options = Object.assign({}, {
            server: this.options.host,
            user: this.options.username,
            password: this.options.password,
            database: this.options.database,
            port: this.options.port
        }, this.options.extra || {});
        // pooling is enabled either when its set explicitly to true,
        // either when its not defined at all (e.g. enabled by default)
        return new Promise((ok, fail) => {
            const connection = new this.mssql.Connection(options).connect((err) => {
                if (err)
                    return fail(err);
                this.connection = connection;
                if (this.options.usePool === false) {
                    this.databaseConnection = {
                        id: 1,
                        connection: new this.mssql.Request(connection),
                        isTransactionActive: false
                    };
                }
                ok();
            });
        });
    }
    /**
     * Closes connection with the database.
     */
    disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.connection)
                throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("mssql");
            this.connection.close();
            this.connection = undefined;
            this.databaseConnection = undefined;
            this.databaseConnectionPool = [];
        });
    }
    /**
     * Creates a query runner used for common queries.
     */
    createQueryRunner() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.connection)
                return Promise.reject(new ConnectionIsNotSetError_1.ConnectionIsNotSetError("mssql"));
            const databaseConnection = yield this.retrieveDatabaseConnection();
            return new SqlServerQueryRunner_1.SqlServerQueryRunner(databaseConnection, this, this.logger);
        });
    }
    /**
     * Access to the native implementation of the database.
     */
    nativeInterface() {
        return {
            driver: this.mssql,
            connection: this.databaseConnection ? this.databaseConnection.connection : undefined,
            pool: this.connection
        };
    }
    /**
     * Replaces parameters in the given sql with special escaping character
     * and an array of parameter names to be passed to a query.
     */
    escapeQueryWithParameters(sql, parameters) {
        if (!parameters || !Object.keys(parameters).length)
            return [sql, []];
        const escapedParameters = [];
        const keys = Object.keys(parameters).map(parameter => "(:" + parameter + "\\b)").join("|");
        sql = sql.replace(new RegExp(keys, "g"), (key) => {
            const value = parameters[key.substr(1)];
            if (value instanceof Array) {
                return value.map((v) => {
                    escapedParameters.push(v);
                    return "@" + (escapedParameters.length - 1);
                }).join(", ");
            }
            else {
                escapedParameters.push(value);
            }
            return "@" + (escapedParameters.length - 1);
        }); // todo: make replace only in value statements, otherwise problems
        return [sql, escapedParameters];
    }
    /**
     * Escapes a column name.
     */
    escapeColumnName(columnName) {
        return `"${columnName}"`;
    }
    /**
     * Escapes an alias.
     */
    escapeAliasName(aliasName) {
        return `"${aliasName}"`;
    }
    /**
     * Escapes a table name.
     */
    escapeTableName(tableName) {
        return `"${tableName}"`;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type and metadata.
     */
    preparePersistentValue(value, column) {
        switch (column.type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value === true ? 1 : 0;
            case ColumnTypes_1.ColumnTypes.DATE:
                return moment(value).format("YYYY-MM-DD");
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value).format("HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.DATETIME:
                return moment(value).format("YYYY-MM-DD HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.stringify(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value
                    .map(i => String(i))
                    .join(",");
        }
        return value;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type or metadata.
     */
    prepareHydratedValue(value, columnOrColumnType) {
        const type = columnOrColumnType instanceof ColumnMetadata_1.ColumnMetadata ? columnOrColumnType.type : columnOrColumnType;
        switch (type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value ? true : false;
            case ColumnTypes_1.ColumnTypes.DATE:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD").toDate();
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value, "HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.DATETIME:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.parse(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value.split(",");
        }
        return value;
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Retrieves a new database connection.
     * If pooling is enabled then connection from the pool will be retrieved.
     * Otherwise active connection will be returned.
     */
    retrieveDatabaseConnection() {
        if (!this.connection)
            throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("mssql");
        return new Promise((ok, fail) => {
            if (this.databaseConnection)
                return ok(this.databaseConnection);
            // let dbConnection: DatabaseConnection|undefined;
            // const connection = this.pool.connect((err: any) => {
            //     if (err)
            //         return fail(err);
            //     ok(dbConnection);
            // });
            //
            // console.log(connection);
            // console.log(this.pool);
            // console.log(this.pool === connection);
            // const request = new this.mssql.Request(this.connection);
            // console.log("request:", request);
            // let dbConnection = this.databaseConnectionPool.find(dbConnection => dbConnection.connection === connection);
            // if (!dbConnection) {
            let dbConnection = {
                id: this.databaseConnectionPool.length,
                connection: this.connection,
                transaction: this.connection.transaction(),
                isTransactionActive: false
            };
            dbConnection.releaseCallback = () => {
                // }
                // if (this.connection && dbConnection) {
                // request.release();
                this.databaseConnectionPool.splice(this.databaseConnectionPool.indexOf(dbConnection), 1);
                return Promise.resolve();
            };
            this.databaseConnectionPool.push(dbConnection);
            ok(dbConnection);
            // }
        });
    }
    /**
     * If driver dependency is not given explicitly, then try to load it via "require".
     */
    loadDependencies() {
        if (!require)
            throw new DriverPackageLoadError_1.DriverPackageLoadError();
        try {
            this.mssql = require("mssql");
        }
        catch (e) {
            throw new DriverPackageNotInstalledError_1.DriverPackageNotInstalledError("SQL Server", "mssql");
        }
    }
}
exports.SqlServerDriver = SqlServerDriver;

//# sourceMappingURL=SqlServerDriver.js.map
