import { QueryRunner } from "../../query-runner/QueryRunner";
import { DatabaseConnection } from "../DatabaseConnection";
import { ObjectLiteral } from "../../common/ObjectLiteral";
import { Logger } from "../../logger/Logger";
import { SqlServerDriver } from "./SqlServerDriver";
import { ColumnSchema } from "../../schema-builder/schema/ColumnSchema";
import { ColumnMetadata } from "../../metadata/ColumnMetadata";
import { TableSchema } from "../../schema-builder/schema/TableSchema";
import { ForeignKeySchema } from "../../schema-builder/schema/ForeignKeySchema";
import { IndexSchema } from "../../schema-builder/schema/IndexSchema";
import { NamingStrategyInterface } from "../../naming-strategy/NamingStrategyInterface";
/**
 * Runs queries on a single mysql database connection.
 */
export declare class SqlServerQueryRunner implements QueryRunner {
    protected databaseConnection: DatabaseConnection;
    protected driver: SqlServerDriver;
    protected logger: Logger;
    /**
     * Indicates if connection for this query runner is released.
     * Once its released, query runner cannot run queries anymore.
     */
    protected isReleased: boolean;
    constructor(databaseConnection: DatabaseConnection, driver: SqlServerDriver, logger: Logger);
    /**
     * Releases database connection. This is needed when using connection pooling.
     * If connection is not from a pool, it should not be released.
     * You cannot use this class's methods after its released.
     */
    release(): Promise<void>;
    /**
     * Removes all tables from the currently connected database.
     */
    clearDatabase(): Promise<void>;
    /**
     * Starts transaction.
     */
    beginTransaction(): Promise<void>;
    /**
     * Commits transaction.
     */
    commitTransaction(): Promise<void>;
    /**
     * Rollbacks transaction.
     */
    rollbackTransaction(): Promise<void>;
    /**
     * Checks if transaction is in progress.
     */
    isTransactionActive(): boolean;
    /**
     * Executes a given SQL query.
     */
    query(query: string, parameters?: any[]): Promise<any>;
    /**
     * Insert a new row with given values into given table.
     */
    insert(tableName: string, keyValues: ObjectLiteral, generatedColumn?: ColumnMetadata): Promise<any>;
    /**
     * Updates rows that match given conditions in the given table.
     */
    update(tableName: string, valuesMap: ObjectLiteral, conditions: ObjectLiteral): Promise<void>;
    /**
     * Deletes from the given table by a given conditions.
     */
    delete(tableName: string, conditions: ObjectLiteral): Promise<void>;
    /**
     * Inserts rows into the closure table.
     */
    insertIntoClosureTable(tableName: string, newEntityId: any, parentId: any, hasLevel: boolean): Promise<number>;
    /**
     * Loads all tables (with given names) from the database and creates a TableSchema from them.
     */
    loadSchemaTables(tableNames: string[], namingStrategy: NamingStrategyInterface): Promise<TableSchema[]>;
    /**
     * Creates a new table from the given table metadata and column metadatas.
     */
    createTable(table: TableSchema): Promise<void>;
    /**
     * Creates a new column from the column metadata in the table.
     */
    createColumns(tableSchema: TableSchema, columns: ColumnSchema[]): Promise<void>;
    /**
     * Changes a column in the table.
     */
    changeColumns(tableSchema: TableSchema, changedColumns: {
        newColumn: ColumnSchema;
        oldColumn: ColumnSchema;
    }[]): Promise<void>;
    /**
     * Drops the columns in the table.
     */
    dropColumns(dbTable: TableSchema, columns: ColumnSchema[]): Promise<void>;
    /**
     * Updates table's primary keys.
     */
    updatePrimaryKeys(dbTable: TableSchema): Promise<void>;
    /**
     * Creates a new foreign keys.
     */
    createForeignKeys(dbTable: TableSchema, foreignKeys: ForeignKeySchema[]): Promise<void>;
    /**
     * Drops a foreign keys from the table.
     */
    dropForeignKeys(tableSchema: TableSchema, foreignKeys: ForeignKeySchema[]): Promise<void>;
    /**
     * Creates a new index.
     */
    createIndex(index: IndexSchema): Promise<void>;
    /**
     * Drops an index from the table.
     */
    dropIndex(tableName: string, indexName: string): Promise<void>;
    /**
     * Creates a database type from a given column metadata.
     */
    normalizeType(column: ColumnMetadata): string;
    /**
     * Database name shortcut.
     */
    protected readonly dbName: string;
    /**
     * Parametrizes given object of values. Used to create column=value queries.
     */
    protected parametrize(objectLiteral: ObjectLiteral): string[];
    /**
     * Builds a query for create column.
     */
    protected buildCreateColumnSql(column: ColumnSchema, skipIdentity?: boolean): string;
}
