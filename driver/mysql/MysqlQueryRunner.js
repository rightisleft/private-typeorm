"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const TransactionAlreadyStartedError_1 = require("../error/TransactionAlreadyStartedError");
const TransactionNotStartedError_1 = require("../error/TransactionNotStartedError");
const DataTypeNotSupportedByDriverError_1 = require("../error/DataTypeNotSupportedByDriverError");
const ColumnSchema_1 = require("../../schema-builder/schema/ColumnSchema");
const TableSchema_1 = require("../../schema-builder/schema/TableSchema");
const ForeignKeySchema_1 = require("../../schema-builder/schema/ForeignKeySchema");
const PrimaryKeySchema_1 = require("../../schema-builder/schema/PrimaryKeySchema");
const IndexSchema_1 = require("../../schema-builder/schema/IndexSchema");
const QueryRunnerAlreadyReleasedError_1 = require("../../query-runner/error/QueryRunnerAlreadyReleasedError");
/**
 * Runs queries on a single mysql database connection.
 */
class MysqlQueryRunner {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(databaseConnection, driver, logger) {
        this.databaseConnection = databaseConnection;
        this.driver = driver;
        this.logger = logger;
        // -------------------------------------------------------------------------
        // Protected Properties
        // -------------------------------------------------------------------------
        /**
         * Indicates if connection for this query runner is released.
         * Once its released, query runner cannot run queries anymore.
         */
        this.isReleased = false;
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Releases database connection. This is needed when using connection pooling.
     * If connection is not from a pool, it should not be released.
     * You cannot use this class's methods after its released.
     */
    release() {
        if (this.databaseConnection.releaseCallback) {
            this.isReleased = true;
            return this.databaseConnection.releaseCallback();
        }
        return Promise.resolve();
    }
    /**
     * Removes all tables from the currently connected database.
     */
    clearDatabase() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const disableForeignKeysCheckQuery = `SET FOREIGN_KEY_CHECKS = 0;`;
            const dropTablesQuery = `SELECT concat('DROP TABLE IF EXISTS ', table_name, ';') AS query FROM information_schema.tables WHERE table_schema = '${this.dbName}'`;
            const enableForeignKeysCheckQuery = `SET FOREIGN_KEY_CHECKS = 1;`;
            yield this.query(disableForeignKeysCheckQuery);
            const dropQueries = yield this.query(dropTablesQuery);
            yield Promise.all(dropQueries.map(query => this.query(query["query"])));
            yield this.query(enableForeignKeysCheckQuery);
        });
    }
    /**
     * Starts transaction.
     */
    beginTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (this.databaseConnection.isTransactionActive)
                throw new TransactionAlreadyStartedError_1.TransactionAlreadyStartedError();
            this.databaseConnection.isTransactionActive = true;
            yield this.query("START TRANSACTION");
        });
    }
    /**
     * Commits transaction.
     */
    commitTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!this.databaseConnection.isTransactionActive)
                throw new TransactionNotStartedError_1.TransactionNotStartedError();
            yield this.query("COMMIT");
            this.databaseConnection.isTransactionActive = false;
        });
    }
    /**
     * Rollbacks transaction.
     */
    rollbackTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!this.databaseConnection.isTransactionActive)
                throw new TransactionNotStartedError_1.TransactionNotStartedError();
            yield this.query("ROLLBACK");
            this.databaseConnection.isTransactionActive = false;
        });
    }
    /**
     * Checks if transaction is in progress.
     */
    isTransactionActive() {
        return this.databaseConnection.isTransactionActive;
    }
    /**
     * Executes a given SQL query.
     */
    query(query, parameters) {
        if (this.isReleased)
            throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
        this.logger.logQuery(query, parameters);
        return new Promise((ok, fail) => {
            this.databaseConnection.connection.query(query, parameters, (err, result) => {
                if (err) {
                    this.logger.logFailedQuery(query, parameters);
                    this.logger.logQueryError(err);
                    return fail(err);
                }
                ok(result);
            });
        });
    }
    /**
     * Insert a new row with given values into given table.
     */
    insert(tableName, keyValues, generatedColumn) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const keys = Object.keys(keyValues);
            const columns = keys.map(key => this.driver.escapeColumnName(key)).join(", ");
            const values = keys.map(key => "?").join(",");
            const parameters = keys.map(key => keyValues[key]);
            const sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(${columns}) VALUES (${values})`;
            const result = yield this.query(sql, parameters);
            return generatedColumn ? result.insertId : undefined;
        });
    }
    /**
     * Updates rows that match given conditions in the given table.
     */
    update(tableName, valuesMap, conditions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const updateValues = this.parametrize(valuesMap).join(", ");
            const conditionString = this.parametrize(conditions).join(" AND ");
            const sql = `UPDATE ${this.driver.escapeTableName(tableName)} SET ${updateValues} ${conditionString ? (" WHERE " + conditionString) : ""}`;
            const conditionParams = Object.keys(conditions).map(key => conditions[key]);
            const updateParams = Object.keys(valuesMap).map(key => valuesMap[key]);
            const allParameters = updateParams.concat(conditionParams);
            yield this.query(sql, allParameters);
        });
    }
    /**
     * Deletes from the given table by a given conditions.
     */
    delete(tableName, conditions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const conditionString = this.parametrize(conditions).join(" AND ");
            const sql = `DELETE FROM ${this.driver.escapeTableName(tableName)} WHERE ${conditionString}`;
            const parameters = Object.keys(conditions).map(key => conditions[key]);
            yield this.query(sql, parameters);
        });
    }
    /**
     * Inserts rows into the closure table.
     */
    insertIntoClosureTable(tableName, newEntityId, parentId, hasLevel) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            let sql = "";
            if (hasLevel) {
                sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(ancestor, descendant, level) ` +
                    `SELECT ancestor, ${newEntityId}, level + 1 FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId} ` +
                    `UNION ALL SELECT ${newEntityId}, ${newEntityId}, 1`;
            }
            else {
                sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(ancestor, descendant) ` +
                    `SELECT ancestor, ${newEntityId} FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId} ` +
                    `UNION ALL SELECT ${newEntityId}, ${newEntityId}`;
            }
            yield this.query(sql);
            const results = yield this.query(`SELECT MAX(level) as level FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId}`);
            return results && results[0] && results[0]["level"] ? parseInt(results[0]["level"]) + 1 : 1;
        });
    }
    /**
     * Loads all tables (with given names) from the database and creates a TableSchema from them.
     */
    loadSchemaTables(tableNames, namingStrategy) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            // if no tables given then no need to proceed
            if (!tableNames || !tableNames.length)
                return [];
            // load tables, columns, indices and foreign keys
            const tablesSql = `SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '${this.dbName}'`;
            const columnsSql = `SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '${this.dbName}'`;
            const indicesSql = `SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = '${this.dbName}' AND INDEX_NAME != 'PRIMARY'`;
            const foreignKeysSql = `SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = '${this.dbName}' AND REFERENCED_COLUMN_NAME IS NOT NULL`;
            const [dbTables, dbColumns, dbIndices, dbForeignKeys] = yield Promise.all([
                this.query(tablesSql),
                this.query(columnsSql),
                this.query(indicesSql),
                this.query(foreignKeysSql)
            ]);
            // if tables were not found in the db, no need to proceed
            if (!dbTables.length)
                return [];
            // create table schemas for loaded tables
            return Promise.all(dbTables.map((dbTable) => __awaiter(this, void 0, void 0, function* () {
                const tableSchema = new TableSchema_1.TableSchema(dbTable["TABLE_NAME"]);
                const primaryKeys = yield this.query(`SHOW INDEX FROM \`${dbTable["TABLE_NAME"]}\` WHERE Key_name = 'PRIMARY'`);
                // create column schemas from the loaded columns
                tableSchema.columns = dbColumns
                    .filter(dbColumn => dbColumn["TABLE_NAME"] === tableSchema.name)
                    .map(dbColumn => {
                    const columnSchema = new ColumnSchema_1.ColumnSchema();
                    columnSchema.name = dbColumn["COLUMN_NAME"];
                    columnSchema.type = dbColumn["COLUMN_TYPE"].toLowerCase();
                    columnSchema.default = dbColumn["COLUMN_DEFAULT"] !== null && dbColumn["COLUMN_DEFAULT"] !== undefined ? dbColumn["COLUMN_DEFAULT"] : undefined;
                    columnSchema.isNullable = dbColumn["IS_NULLABLE"] === "YES";
                    columnSchema.isPrimary = dbColumn["COLUMN_KEY"].indexOf("PRI") !== -1;
                    columnSchema.isUnique = dbColumn["COLUMN_KEY"].indexOf("UNI") !== -1;
                    columnSchema.isGenerated = dbColumn["EXTRA"].indexOf("auto_increment") !== -1;
                    columnSchema.comment = dbColumn["COLUMN_COMMENT"];
                    return columnSchema;
                });
                // create primary keys
                tableSchema.primaryKeys = primaryKeys.map(primaryKey => {
                    return new PrimaryKeySchema_1.PrimaryKeySchema(primaryKey["Key_name"], primaryKey["Column_name"]);
                });
                // create foreign key schemas from the loaded indices
                tableSchema.foreignKeys = dbForeignKeys
                    .filter(dbForeignKey => dbForeignKey["TABLE_NAME"] === tableSchema.name)
                    .map(dbForeignKey => new ForeignKeySchema_1.ForeignKeySchema(dbForeignKey["CONSTRAINT_NAME"], [], [], "", "")); // todo: fix missing params
                // create index schemas from the loaded indices
                tableSchema.indices = dbIndices
                    .filter(dbIndex => {
                    return dbIndex["TABLE_NAME"] === tableSchema.name &&
                        (!tableSchema.foreignKeys.find(foreignKey => foreignKey.name === dbIndex["INDEX_NAME"])) &&
                        (!tableSchema.primaryKeys.find(primaryKey => primaryKey.name === dbIndex["INDEX_NAME"]));
                })
                    .map(dbIndex => dbIndex["INDEX_NAME"])
                    .filter((value, index, self) => self.indexOf(value) === index) // unqiue
                    .map(dbIndexName => {
                    const currentDbIndices = dbIndices.filter(dbIndex => dbIndex["TABLE_NAME"] === tableSchema.name && dbIndex["INDEX_NAME"] === dbIndexName);
                    const columnNames = currentDbIndices.map(dbIndex => dbIndex["COLUMN_NAME"]);
                    // find a special index - unique index and
                    if (currentDbIndices.length === 1 && currentDbIndices[0]["NON_UNIQUE"] === 0) {
                        const column = tableSchema.columns.find(column => column.name === currentDbIndices[0]["INDEX_NAME"] && column.name === currentDbIndices[0]["COLUMN_NAME"]);
                        if (column) {
                            column.isUnique = true;
                            return;
                        }
                    }
                    return new IndexSchema_1.IndexSchema(dbTable["TABLE_NAME"], dbIndexName, columnNames, false /* todo: uniqueness */);
                })
                    .filter(index => !!index); // remove empty returns
                return tableSchema;
            })));
        });
    }
    /**
     * Creates a new table from the given table metadata and column metadatas.
     */
    createTable(table) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const columnDefinitions = table.columns.map(column => this.buildCreateColumnSql(column, false)).join(", ");
            let sql = `CREATE TABLE \`${table.name}\` (${columnDefinitions}`;
            const primaryKeyColumns = table.columns.filter(column => column.isPrimary && !column.isGenerated);
            if (primaryKeyColumns.length > 0)
                sql += `, PRIMARY KEY(${primaryKeyColumns.map(column => `\`${column.name}\``).join(", ")})`;
            sql += `) ENGINE=InnoDB;`; // todo: remove engine from here
            yield this.query(sql);
        });
    }
    /**
     * Creates a new column from the column metadata in the table.
     */
    createColumns(tableSchema, columns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const queries = columns.map(column => {
                const sql = `ALTER TABLE \`${tableSchema.name}\` ADD ${this.buildCreateColumnSql(column, false)}`;
                return this.query(sql);
            });
            yield Promise.all(queries);
        });
    }
    /**
     * Changes a column in the table.
     */
    changeColumns(tableSchema, changedColumns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const updatePromises = changedColumns.map((changedColumn) => __awaiter(this, void 0, void 0, function* () {
                const sql = `ALTER TABLE \`${tableSchema.name}\` CHANGE \`${changedColumn.oldColumn.name}\` ${this.buildCreateColumnSql(changedColumn.newColumn, changedColumn.oldColumn.isPrimary)}`; // todo: CHANGE OR MODIFY COLUMN ????
                if (changedColumn.newColumn.isUnique === false && changedColumn.oldColumn.isUnique === true)
                    yield this.query(`ALTER TABLE \`${tableSchema.name}\` DROP INDEX \`${changedColumn.oldColumn.name}\``);
                return this.query(sql);
            }));
            yield Promise.all(updatePromises);
        });
    }
    /**
     * Drops the columns in the table.
     */
    dropColumns(dbTable, columns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const dropPromises = columns.map(column => {
                return this.query(`ALTER TABLE \`${dbTable.name}\` DROP \`${column.name}\``);
            });
            yield Promise.all(dropPromises);
        });
    }
    /**
     * Updates table's primary keys.
     */
    updatePrimaryKeys(tableSchema) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!tableSchema.hasGeneratedColumn)
                yield this.query(`ALTER TABLE ${tableSchema.name} DROP PRIMARY KEY`);
            const primaryColumnNames = tableSchema.columns.filter(column => column.isPrimary && !column.isGenerated).map(column => "`" + column.name + "`");
            if (primaryColumnNames.length > 0)
                yield this.query(`ALTER TABLE ${tableSchema.name} ADD PRIMARY KEY (${primaryColumnNames.join(", ")})`);
        });
    }
    /**
     * Creates a new foreign keys.
     */
    createForeignKeys(dbTable, foreignKeys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const promises = foreignKeys.map(foreignKey => {
                const columnNames = foreignKey.columnNames.map(column => "`" + column + "`").join(", ");
                const referencedColumnNames = foreignKey.referencedColumnNames.map(column => "`" + column + "`").join(",");
                let sql = `ALTER TABLE ${dbTable.name} ADD CONSTRAINT \`${foreignKey.name}\` ` +
                    `FOREIGN KEY (${columnNames}) ` +
                    `REFERENCES \`${foreignKey.referencedTableName}\`(${referencedColumnNames})`;
                if (foreignKey.onDelete)
                    sql += " ON DELETE " + foreignKey.onDelete;
                return this.query(sql);
            });
            yield Promise.all(promises);
        });
    }
    /**
     * Drops a foreign keys from the table.
     */
    dropForeignKeys(tableSchema, foreignKeys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const promises = foreignKeys.map(foreignKey => {
                const sql = `ALTER TABLE \`${tableSchema.name}\` DROP FOREIGN KEY \`${foreignKey.name}\``;
                return this.query(sql);
            });
            yield Promise.all(promises);
        });
    }
    /**
     * Creates a new index.
     */
    createIndex(index) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const columns = index.columnNames.map(columnName => "`" + columnName + "`").join(", ");
            const sql = `CREATE ${index.isUnique ? "UNIQUE " : ""}INDEX \`${index.name}\` ON \`${index.tableName}\`(${columns})`;
            yield this.query(sql);
        });
    }
    /**
     * Drops an index from the table.
     */
    dropIndex(tableName, indexName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const sql = `ALTER TABLE \`${tableName}\` DROP INDEX \`${indexName}\``;
            yield this.query(sql);
        });
    }
    /**
     * Creates a database type from a given column metadata.
     */
    normalizeType(column) {
        switch (column.normalizedDataType) {
            case "string":
                return "varchar(" + (column.length ? column.length : 255) + ")";
            case "text":
                return "text";
            case "boolean":
                return "tinyint(1)";
            case "integer":
            case "int":
                return "int(" + (column.length ? column.length : 11) + ")";
            case "smallint":
                return "smallint(" + (column.length ? column.length : 11) + ")";
            case "bigint":
                return "bigint(" + (column.length ? column.length : 11) + ")";
            case "float":
                return "float";
            case "double":
            case "number":
                return "double";
            case "decimal":
                if (column.precision && column.scale) {
                    return `decimal(${column.precision},${column.scale})`;
                }
                else if (column.scale) {
                    return `decimal(${column.scale})`;
                }
                else if (column.precision) {
                    return `decimal(${column.precision})`;
                }
                else {
                    return "decimal";
                }
            case "date":
                return "date";
            case "time":
                return "time";
            case "datetime":
                return "datetime";
            case "json":
                return "text";
            case "simple_array":
                return column.length ? "varchar(" + column.length + ")" : "text";
        }
        throw new DataTypeNotSupportedByDriverError_1.DataTypeNotSupportedByDriverError(column.type, "MySQL");
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Database name shortcut.
     */
    get dbName() {
        return this.driver.options.database;
    }
    /**
     * Parametrizes given object of values. Used to create column=value queries.
     */
    parametrize(objectLiteral) {
        return Object.keys(objectLiteral).map(key => this.driver.escapeColumnName(key) + "=?");
    }
    /**
     * Builds a query for create column.
     */
    buildCreateColumnSql(column, skipPrimary) {
        let c = "`" + column.name + "` " + column.type;
        if (column.isNullable !== true)
            c += " NOT NULL";
        if (column.isUnique === true)
            c += " UNIQUE";
        if (column.isGenerated && column.isPrimary && !skipPrimary)
            c += " PRIMARY KEY";
        if (column.isGenerated === true)
            c += " AUTO_INCREMENT";
        if (column.comment)
            c += " COMMENT '" + column.comment + "'";
        if (column.default)
            c += " DEFAULT " + column.default + "";
        return c;
    }
}
exports.MysqlQueryRunner = MysqlQueryRunner;

//# sourceMappingURL=MysqlQueryRunner.js.map
