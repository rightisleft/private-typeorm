"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const ConnectionIsNotSetError_1 = require("../error/ConnectionIsNotSetError");
const DriverPackageNotInstalledError_1 = require("../error/DriverPackageNotInstalledError");
const DriverPackageLoadError_1 = require("../error/DriverPackageLoadError");
const DriverUtils_1 = require("../DriverUtils");
const MysqlQueryRunner_1 = require("./MysqlQueryRunner");
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const moment = require("moment");
const ColumnMetadata_1 = require("../../metadata/ColumnMetadata");
const DriverOptionNotSetError_1 = require("../error/DriverOptionNotSetError");
/**
 * Organizes communication with MySQL DBMS.
 */
class MysqlDriver {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(options, logger, mysql, mysqlVersion = "mysql") {
        /**
         * Pool of database connections.
         */
        this.databaseConnectionPool = [];
        /**
         * Driver type's version. node-mysql and mysql2 are supported.
         */
        this.version = "mysql";
        this.options = DriverUtils_1.DriverUtils.buildDriverOptions(options);
        this.logger = logger;
        this.mysql = mysql;
        this.version = mysqlVersion;
        // validate options to make sure everything is set
        if (!this.options.host)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("host");
        if (!this.options.username)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("username");
        if (!this.options.database)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("database");
        // if mysql package instance was not set explicitly then try to load it
        if (!mysql)
            this.loadDependencies();
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Performs connection to the database.
     * Based on pooling options, it can either create connection immediately,
     * either create a pool and create connection when needed.
     */
    connect() {
        // build connection options for the driver
        const options = Object.assign({}, {
            host: this.options.host,
            user: this.options.username,
            password: this.options.password,
            database: this.options.database,
            port: this.options.port
        }, this.options.extra || {});
        // pooling is enabled either when its set explicitly to true,
        // either when its not defined at all (e.g. enabled by default)
        if (this.options.usePool === undefined || this.options.usePool === true) {
            this.pool = this.mysql.createPool(options);
            return Promise.resolve();
        }
        else {
            return new Promise((ok, fail) => {
                const connection = this.mysql.createConnection(options);
                this.databaseConnection = {
                    id: 1,
                    connection: connection,
                    isTransactionActive: false
                };
                this.databaseConnection.connection.connect((err) => err ? fail(err) : ok());
            });
        }
    }
    /**
     * Closes connection with the database.
     */
    disconnect() {
        if (!this.databaseConnection && !this.pool)
            throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError(this.version);
        return new Promise((ok, fail) => {
            const handler = (err) => err ? fail(err) : ok();
            // if pooling is used, then disconnect from it
            if (this.pool) {
                this.pool.end(handler);
                this.pool = undefined;
                this.databaseConnectionPool = [];
            }
            // if single connection is opened, then close it
            if (this.databaseConnection) {
                this.databaseConnection.connection.end(handler);
                this.databaseConnection = undefined;
            }
        });
    }
    /**
     * Creates a query runner used for common queries.
     */
    createQueryRunner() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.databaseConnection && !this.pool)
                return Promise.reject(new ConnectionIsNotSetError_1.ConnectionIsNotSetError(this.version));
            const databaseConnection = yield this.retrieveDatabaseConnection();
            return new MysqlQueryRunner_1.MysqlQueryRunner(databaseConnection, this, this.logger);
        });
    }
    /**
     * Access to the native implementation of the database.
     */
    nativeInterface() {
        return {
            driver: this.mysql,
            connection: this.databaseConnection ? this.databaseConnection.connection : undefined,
            pool: this.pool
        };
    }
    /**
     * Replaces parameters in the given sql with special escaping character
     * and an array of parameter names to be passed to a query.
     */
    escapeQueryWithParameters(sql, parameters) {
        if (!parameters || !Object.keys(parameters).length)
            return [sql, []];
        const escapedParameters = [];
        const keys = Object.keys(parameters).map(parameter => "(:" + parameter + "\\b)").join("|");
        sql = sql.replace(new RegExp(keys, "g"), (key) => {
            escapedParameters.push(parameters[key.substr(1)]);
            return "?";
        }); // todo: make replace only in value statements, otherwise problems
        return [sql, escapedParameters];
    }
    /**
     * Escapes a column name.
     */
    escapeColumnName(columnName) {
        return columnName; // "`" + columnName + "`";
    }
    /**
     * Escapes an alias.
     */
    escapeAliasName(aliasName) {
        return aliasName; // "`" + aliasName + "`";
    }
    /**
     * Escapes a table name.
     */
    escapeTableName(tableName) {
        return tableName; // "`" + tableName + "`";
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type and metadata.
     */
    preparePersistentValue(value, column) {
        switch (column.type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value === true ? 1 : 0;
            case ColumnTypes_1.ColumnTypes.DATE:
                if (moment(value).isValid())
                    return moment(value).format("YYYY-MM-DD");
                else
                    return "0000-00-00";
            case ColumnTypes_1.ColumnTypes.TIME:
                if (moment(value).isValid())
                    return moment(value).format("HH:mm:ss");
                else
                    return "00:00:00";
            case ColumnTypes_1.ColumnTypes.DATETIME:
                if (moment(value).isValid())
                    return moment(value).format("YYYY-MM-DD HH:mm:ss");
                else
                    return "0000-00-00 00:00:00";
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.stringify(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value
                    .map(i => String(i))
                    .join(",");
        }
        return value;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type or metadata.
     */
    prepareHydratedValue(value, columnOrColumnType) {
        const type = columnOrColumnType instanceof ColumnMetadata_1.ColumnMetadata ? columnOrColumnType.type : columnOrColumnType;
        switch (type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value ? true : false;
            case ColumnTypes_1.ColumnTypes.DATE:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD").toDate();
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value, "HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.DATETIME:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.parse(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value.split(",");
        }
        return value;
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Retrieves a new database connection.
     * If pooling is enabled then connection from the pool will be retrieved.
     * Otherwise active connection will be returned.
     */
    retrieveDatabaseConnection() {
        if (this.pool) {
            return new Promise((ok, fail) => {
                this.pool.getConnection((err, connection) => {
                    if (err)
                        return fail(err);
                    let dbConnection = this.databaseConnectionPool.find(dbConnection => dbConnection.connection === connection);
                    if (!dbConnection) {
                        dbConnection = {
                            id: this.databaseConnectionPool.length,
                            connection: connection,
                            isTransactionActive: false
                        };
                        dbConnection.releaseCallback = () => {
                            if (this.pool && dbConnection) {
                                connection.release();
                                this.databaseConnectionPool.splice(this.databaseConnectionPool.indexOf(dbConnection), 1);
                            }
                            return Promise.resolve();
                        };
                        this.databaseConnectionPool.push(dbConnection);
                    }
                    ok(dbConnection);
                });
            });
        }
        if (this.databaseConnection)
            return Promise.resolve(this.databaseConnection);
        throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError(this.version);
    }
    /**
     * If driver dependency is not given explicitly, then try to load it via "require".
     */
    loadDependencies() {
        if (!require)
            throw new DriverPackageLoadError_1.DriverPackageLoadError();
        try {
            this.mysql = require(this.version);
        }
        catch (e) {
            throw new DriverPackageNotInstalledError_1.DriverPackageNotInstalledError("Mysql", this.version);
        }
    }
}
exports.MysqlDriver = MysqlDriver;

//# sourceMappingURL=MysqlDriver.js.map
