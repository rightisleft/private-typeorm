import { Driver } from "../Driver";
import { DriverOptions } from "../DriverOptions";
import { DatabaseConnection } from "../DatabaseConnection";
import { Logger } from "../../logger/Logger";
import { QueryRunner } from "../../query-runner/QueryRunner";
import { ColumnType } from "../../metadata/types/ColumnTypes";
import { ObjectLiteral } from "../../common/ObjectLiteral";
import { ColumnMetadata } from "../../metadata/ColumnMetadata";
/**
 * Organizes communication with MySQL DBMS.
 */
export declare class MysqlDriver implements Driver {
    /**
     * Driver connection options.
     */
    readonly options: DriverOptions;
    /**
     * Mysql library.
     */
    protected mysql: any;
    /**
     * Connection to mysql database.
     */
    protected databaseConnection: DatabaseConnection | undefined;
    /**
     * Mysql pool.
     */
    protected pool: any;
    /**
     * Pool of database connections.
     */
    protected databaseConnectionPool: DatabaseConnection[];
    /**
     * Logger used go log queries and errors.
     */
    protected logger: Logger;
    /**
     * Driver type's version. node-mysql and mysql2 are supported.
     */
    protected version: "mysql" | "mysql2";
    constructor(options: DriverOptions, logger: Logger, mysql?: any, mysqlVersion?: "mysql" | "mysql2");
    /**
     * Performs connection to the database.
     * Based on pooling options, it can either create connection immediately,
     * either create a pool and create connection when needed.
     */
    connect(): Promise<void>;
    /**
     * Closes connection with the database.
     */
    disconnect(): Promise<void>;
    /**
     * Creates a query runner used for common queries.
     */
    createQueryRunner(): Promise<QueryRunner>;
    /**
     * Access to the native implementation of the database.
     */
    nativeInterface(): {
        driver: any;
        connection: any;
        pool: any;
    };
    /**
     * Replaces parameters in the given sql with special escaping character
     * and an array of parameter names to be passed to a query.
     */
    escapeQueryWithParameters(sql: string, parameters: ObjectLiteral): [string, any[]];
    /**
     * Escapes a column name.
     */
    escapeColumnName(columnName: string): string;
    /**
     * Escapes an alias.
     */
    escapeAliasName(aliasName: string): string;
    /**
     * Escapes a table name.
     */
    escapeTableName(tableName: string): string;
    /**
     * Prepares given value to a value to be persisted, based on its column type and metadata.
     */
    preparePersistentValue(value: any, column: ColumnMetadata): any;
    /**
     * Prepares given value to a value to be persisted, based on its column metadata.
     */
    prepareHydratedValue(value: any, type: ColumnType): any;
    /**
     * Prepares given value to a value to be persisted, based on its column type.
     */
    prepareHydratedValue(value: any, column: ColumnMetadata): any;
    /**
     * Retrieves a new database connection.
     * If pooling is enabled then connection from the pool will be retrieved.
     * Otherwise active connection will be returned.
     */
    protected retrieveDatabaseConnection(): Promise<DatabaseConnection>;
    /**
     * If driver dependency is not given explicitly, then try to load it via "require".
     */
    protected loadDependencies(): void;
}
