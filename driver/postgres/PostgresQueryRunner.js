"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const TransactionAlreadyStartedError_1 = require("../error/TransactionAlreadyStartedError");
const TransactionNotStartedError_1 = require("../error/TransactionNotStartedError");
const DataTypeNotSupportedByDriverError_1 = require("../error/DataTypeNotSupportedByDriverError");
const ColumnSchema_1 = require("../../schema-builder/schema/ColumnSchema");
const TableSchema_1 = require("../../schema-builder/schema/TableSchema");
const IndexSchema_1 = require("../../schema-builder/schema/IndexSchema");
const ForeignKeySchema_1 = require("../../schema-builder/schema/ForeignKeySchema");
const PrimaryKeySchema_1 = require("../../schema-builder/schema/PrimaryKeySchema");
const QueryRunnerAlreadyReleasedError_1 = require("../../query-runner/error/QueryRunnerAlreadyReleasedError");
/**
 * Runs queries on a single postgres database connection.
 */
class PostgresQueryRunner {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(databaseConnection, driver, logger) {
        this.databaseConnection = databaseConnection;
        this.driver = driver;
        this.logger = logger;
        // -------------------------------------------------------------------------
        // Protected Properties
        // -------------------------------------------------------------------------
        /**
         * Indicates if connection for this query runner is released.
         * Once its released, query runner cannot run queries anymore.
         */
        this.isReleased = false;
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Releases database connection. This is needed when using connection pooling.
     * If connection is not from a pool, it should not be released.
     */
    release() {
        if (this.databaseConnection.releaseCallback) {
            this.isReleased = true;
            return this.databaseConnection.releaseCallback();
        }
        return Promise.resolve();
    }
    /**
     * Removes all tables from the currently connected database.
     */
    clearDatabase() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const selectDropsQuery = `SELECT 'DROP TABLE IF EXISTS "' || tablename || '" CASCADE;' as query FROM pg_tables WHERE schemaname = 'public'`;
            const dropQueries = yield this.query(selectDropsQuery);
            yield Promise.all(dropQueries.map(q => this.query(q["query"])));
        });
    }
    /**
     * Starts transaction.
     */
    beginTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (this.databaseConnection.isTransactionActive)
                throw new TransactionAlreadyStartedError_1.TransactionAlreadyStartedError();
            this.databaseConnection.isTransactionActive = true;
            yield this.query("START TRANSACTION");
        });
    }
    /**
     * Commits transaction.
     */
    commitTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!this.databaseConnection.isTransactionActive)
                throw new TransactionNotStartedError_1.TransactionNotStartedError();
            yield this.query("COMMIT");
            this.databaseConnection.isTransactionActive = false;
        });
    }
    /**
     * Rollbacks transaction.
     */
    rollbackTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!this.databaseConnection.isTransactionActive)
                throw new TransactionNotStartedError_1.TransactionNotStartedError();
            yield this.query("ROLLBACK");
            this.databaseConnection.isTransactionActive = false;
        });
    }
    /**
     * Checks if transaction is in progress.
     */
    isTransactionActive() {
        return this.databaseConnection.isTransactionActive;
    }
    /**
     * Executes a given SQL query.
     */
    query(query, parameters) {
        if (this.isReleased)
            throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
        // console.log("query: ", query);
        // console.log("parameters: ", parameters);
        this.logger.logQuery(query, parameters);
        return new Promise((ok, fail) => {
            this.databaseConnection.connection.query(query, parameters, (err, result) => {
                if (err) {
                    this.logger.logFailedQuery(query, parameters);
                    this.logger.logQueryError(err);
                    fail(err);
                }
                else {
                    ok(result.rows);
                }
            });
        });
    }
    /**
     * Insert a new row into given table.
     */
    insert(tableName, keyValues, generatedColumn) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const keys = Object.keys(keyValues);
            const columns = keys.map(key => this.driver.escapeColumnName(key)).join(", ");
            const values = keys.map((key, index) => "$" + (index + 1)).join(",");
            const sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(${columns}) VALUES (${values}) ${generatedColumn ? " RETURNING " + this.driver.escapeColumnName(generatedColumn.name) : ""}`;
            const parameters = keys.map(key => keyValues[key]);
            const result = yield this.query(sql, parameters);
            if (generatedColumn)
                return result[0][generatedColumn.name];
            return result;
        });
    }
    /**
     * Updates rows that match given conditions in the given table.
     */
    update(tableName, valuesMap, conditions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const updateValues = this.parametrize(valuesMap).join(", ");
            const conditionString = this.parametrize(conditions, Object.keys(valuesMap).length).join(" AND ");
            const query = `UPDATE ${this.driver.escapeTableName(tableName)} SET ${updateValues} ${conditionString ? (" WHERE " + conditionString) : ""}`;
            const updateParams = Object.keys(valuesMap).map(key => valuesMap[key]);
            const conditionParams = Object.keys(conditions).map(key => conditions[key]);
            const allParameters = updateParams.concat(conditionParams);
            yield this.query(query, allParameters);
        });
    }
    /**
     * Deletes from the given table by a given conditions.
     */
    delete(tableName, conditions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const conditionString = this.parametrize(conditions).join(" AND ");
            const parameters = Object.keys(conditions).map(key => conditions[key]);
            const query = `DELETE FROM "${tableName}" WHERE ${conditionString}`;
            yield this.query(query, parameters);
        });
    }
    /**
     * Inserts rows into closure table.
     */
    insertIntoClosureTable(tableName, newEntityId, parentId, hasLevel) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            let sql = "";
            if (hasLevel) {
                sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(ancestor, descendant, level) ` +
                    `SELECT ancestor, ${newEntityId}, level + 1 FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId} ` +
                    `UNION ALL SELECT ${newEntityId}, ${newEntityId}, 1`;
            }
            else {
                sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(ancestor, descendant) ` +
                    `SELECT ancestor, ${newEntityId} FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId} ` +
                    `UNION ALL SELECT ${newEntityId}, ${newEntityId}`;
            }
            yield this.query(sql);
            const results = yield this.query(`SELECT MAX(level) as level FROM ${tableName} WHERE descendant = ${parentId}`);
            return results && results[0] && results[0]["level"] ? parseInt(results[0]["level"]) + 1 : 1;
        });
    }
    /**
     * Loads all tables (with given names) from the database and creates a TableSchema from them.
     */
    loadSchemaTables(tableNames, namingStrategy) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            // if no tables given then no need to proceed
            if (!tableNames || !tableNames.length)
                return [];
            // load tables, columns, indices and foreign keys
            const tableNamesString = tableNames.map(name => "'" + name + "'").join(", ");
            const tablesSql = `SELECT * FROM information_schema.tables WHERE table_catalog = '${this.dbName}' AND table_schema = 'public'`;
            const columnsSql = `SELECT * FROM information_schema.columns WHERE table_catalog = '${this.dbName}' AND table_schema = 'public'`;
            const indicesSql = `SELECT t.relname AS table_name, i.relname AS index_name, a.attname AS column_name  FROM pg_class t, pg_class i, pg_index ix, pg_attribute a
WHERE t.oid = ix.indrelid AND i.oid = ix.indexrelid AND a.attrelid = t.oid
AND a.attnum = ANY(ix.indkey) AND t.relkind = 'r' AND t.relname IN (${tableNamesString}) ORDER BY t.relname, i.relname`;
            const foreignKeysSql = `SELECT table_name, constraint_name FROM information_schema.table_constraints WHERE table_catalog = '${this.dbName}' AND constraint_type = 'FOREIGN KEY'`;
            const uniqueKeysSql = `SELECT * FROM information_schema.table_constraints WHERE table_catalog = '${this.dbName}' AND constraint_type = 'UNIQUE'`;
            const primaryKeysSql = `SELECT c.column_name, tc.table_name, tc.constraint_name FROM information_schema.table_constraints tc
JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name)
JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
where constraint_type = 'PRIMARY KEY' and tc.table_catalog = '${this.dbName}'`;
            const [dbTables, dbColumns, dbIndices, dbForeignKeys, dbUniqueKeys, primaryKeys] = yield Promise.all([
                this.query(tablesSql),
                this.query(columnsSql),
                this.query(indicesSql),
                this.query(foreignKeysSql),
                this.query(uniqueKeysSql),
                this.query(primaryKeysSql),
            ]);
            // if tables were not found in the db, no need to proceed
            if (!dbTables.length)
                return [];
            // create table schemas for loaded tables
            return dbTables.map(dbTable => {
                const tableSchema = new TableSchema_1.TableSchema(dbTable["table_name"]);
                // create column schemas from the loaded columns
                tableSchema.columns = dbColumns
                    .filter(dbColumn => dbColumn["table_name"] === tableSchema.name)
                    .map(dbColumn => {
                    const columnType = dbColumn["data_type"].toLowerCase() + (dbColumn["character_maximum_length"] !== undefined && dbColumn["character_maximum_length"] !== null ? ("(" + dbColumn["character_maximum_length"] + ")") : "");
                    const isGenerated = dbColumn["column_default"] === `nextval('${dbColumn["table_name"]}_id_seq'::regclass)` || dbColumn["column_default"] === `nextval('"${dbColumn["table_name"]}_id_seq"'::regclass)`;
                    const columnSchema = new ColumnSchema_1.ColumnSchema();
                    columnSchema.name = dbColumn["column_name"];
                    columnSchema.type = columnType;
                    columnSchema.default = dbColumn["column_default"] !== null && dbColumn["column_default"] !== undefined ? dbColumn["column_default"] : undefined;
                    columnSchema.isNullable = dbColumn["is_nullable"] === "YES";
                    // columnSchema.isPrimary = dbColumn["column_key"].indexOf("PRI") !== -1;
                    columnSchema.isGenerated = isGenerated;
                    columnSchema.comment = ""; // dbColumn["COLUMN_COMMENT"];
                    columnSchema.isUnique = !!dbUniqueKeys.find(key => key["constraint_name"] === "uk_" + dbColumn["column_name"]);
                    return columnSchema;
                });
                // create primary key schema
                tableSchema.primaryKeys = primaryKeys
                    .filter(primaryKey => primaryKey["table_name"] === tableSchema.name)
                    .map(primaryKey => new PrimaryKeySchema_1.PrimaryKeySchema(primaryKey["constraint_name"], primaryKey["column_name"]));
                // create foreign key schemas from the loaded indices
                tableSchema.foreignKeys = dbForeignKeys
                    .filter(dbForeignKey => dbForeignKey["table_name"] === tableSchema.name)
                    .map(dbForeignKey => new ForeignKeySchema_1.ForeignKeySchema(dbForeignKey["constraint_name"], [], [], "", "")); // todo: fix missing params
                // create unique key schemas from the loaded indices
                /*tableSchema.uniqueKeys = dbUniqueKeys
                    .filter(dbUniqueKey => dbUniqueKey["table_name"] === tableSchema.name)
                    .map(dbUniqueKey => {
                        return new UniqueKeySchema(dbUniqueKey["TABLE_NAME"], dbUniqueKey["CONSTRAINT_NAME"], [/!* todo *!/]);
                    });*/
                // create index schemas from the loaded indices
                tableSchema.indices = dbIndices
                    .filter(dbIndex => {
                    return dbIndex["table_name"] === tableSchema.name &&
                        (!tableSchema.foreignKeys.find(foreignKey => foreignKey.name === dbIndex["index_name"])) &&
                        (!tableSchema.primaryKeys.find(primaryKey => primaryKey.name === dbIndex["index_name"])) &&
                        (!dbUniqueKeys.find(key => key["constraint_name"] === dbIndex["index_name"]));
                })
                    .map(dbIndex => dbIndex["index_name"])
                    .filter((value, index, self) => self.indexOf(value) === index) // unqiue
                    .map(dbIndexName => {
                    const columnNames = dbIndices
                        .filter(dbIndex => dbIndex["table_name"] === tableSchema.name && dbIndex["index_name"] === dbIndexName)
                        .map(dbIndex => dbIndex["column_name"]);
                    return new IndexSchema_1.IndexSchema(dbTable["TABLE_NAME"], dbIndexName, columnNames, false /* todo: uniqueness */);
                });
                return tableSchema;
            });
        });
    }
    /**
     * Creates a new table from the given table metadata and column metadatas.
     */
    createTable(table) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const columnDefinitions = table.columns.map(column => this.buildCreateColumnSql(column, false)).join(", ");
            let sql = `CREATE TABLE "${table.name}" (${columnDefinitions}`;
            sql += table.columns
                .filter(column => column.isUnique)
                .map(column => `, CONSTRAINT "uk_${column.name}" UNIQUE ("${column.name}")`)
                .join(" ");
            const primaryKeyColumns = table.columns.filter(column => column.isPrimary && !column.isGenerated);
            if (primaryKeyColumns.length > 0)
                sql += `, PRIMARY KEY(${primaryKeyColumns.map(column => `"${column.name}"`).join(", ")})`;
            sql += `)`;
            yield this.query(sql);
        });
    }
    /**
     * Creates a new column from the column metadata in the table.
     */
    createColumns(tableSchema, columns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const queries = columns.map(column => {
                const sql = `ALTER TABLE "${tableSchema.name}" ADD ${this.buildCreateColumnSql(column, false)}`;
                return this.query(sql);
            });
            yield Promise.all(queries);
        });
    }
    /**
     * Changes a column in the table.
     */
    changeColumns(tableSchema, changedColumns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const updatePromises = changedColumns.map((changedColumn) => __awaiter(this, void 0, void 0, function* () {
                const oldColumn = changedColumn.oldColumn;
                const newColumn = changedColumn.newColumn;
                if (oldColumn.type !== newColumn.type ||
                    oldColumn.name !== newColumn.name) {
                    let sql = `ALTER TABLE "${tableSchema.name}" ALTER COLUMN "${oldColumn.name}"`;
                    if (oldColumn.type !== newColumn.type) {
                        sql += ` TYPE ${newColumn.type}`;
                    }
                    if (oldColumn.name !== newColumn.name) {
                        sql += ` RENAME TO ` + newColumn.name;
                    }
                    yield this.query(sql);
                }
                if (oldColumn.isNullable !== newColumn.isNullable) {
                    let sql = `ALTER TABLE "${tableSchema.name}" ALTER COLUMN "${oldColumn.name}"`;
                    if (newColumn.isNullable) {
                        sql += ` DROP NOT NULL`;
                    }
                    else {
                        sql += ` SET NOT NULL`;
                    }
                    yield this.query(sql);
                }
                // update sequence generation
                if (oldColumn.isGenerated !== newColumn.isGenerated) {
                    if (!oldColumn.isGenerated) {
                        yield this.query(`CREATE SEQUENCE "${tableSchema.name}_id_seq" OWNED BY "${tableSchema.name}"."${oldColumn.name}"`);
                        yield this.query(`ALTER TABLE "${tableSchema.name}" ALTER COLUMN "${oldColumn.name}" SET DEFAULT nextval('"${tableSchema.name}_id_seq"')`);
                    }
                    else {
                        yield this.query(`ALTER TABLE "${tableSchema.name}" ALTER COLUMN "${oldColumn.name}" DROP DEFAULT`);
                        yield this.query(`DROP SEQUENCE "${tableSchema.name}_id_seq"`);
                    }
                }
                if (oldColumn.comment !== newColumn.comment) {
                    yield this.query(`COMMENT ON COLUMN "${tableSchema.name}"."${oldColumn.name}" is '${newColumn.comment}'`);
                }
                if (oldColumn.isUnique !== newColumn.isUnique) {
                    if (newColumn.isUnique === true) {
                        yield this.query(`ALTER TABLE "${tableSchema.name}" ADD CONSTRAINT "uk_${newColumn.name}" UNIQUE ("${newColumn.name}")`);
                    }
                    else if (newColumn.isUnique === false) {
                        yield this.query(`ALTER TABLE "${tableSchema.name}" DROP CONSTRAINT "uk_${newColumn.name}"`);
                    }
                }
            }));
            yield Promise.all(updatePromises);
        });
    }
    /**
     * Drops the columns in the table.
     */
    dropColumns(dbTable, columns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const dropPromises = columns.map(column => {
                return this.query(`ALTER TABLE "${dbTable.name}" DROP "${column.name}"`);
            });
            yield Promise.all(dropPromises);
        });
    }
    /**
     * Updates table's primary keys.
     */
    updatePrimaryKeys(dbTable) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const primaryColumnNames = dbTable.primaryKeys.map(primaryKey => `"${primaryKey.columnName}"`);
            yield this.query(`ALTER TABLE "${dbTable.name}" DROP CONSTRAINT IF EXISTS "${dbTable.name}_pkey"`);
            yield this.query(`DROP INDEX IF EXISTS "${dbTable.name}_pkey"`);
            if (primaryColumnNames.length > 0)
                yield this.query(`ALTER TABLE "${dbTable.name}" ADD PRIMARY KEY (${primaryColumnNames.join(", ")})`);
        });
    }
    /**
     * Creates a new foreign keys.
     */
    createForeignKeys(dbTable, foreignKeys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const promises = foreignKeys.map(foreignKey => {
                let sql = `ALTER TABLE "${dbTable.name}" ADD CONSTRAINT "${foreignKey.name}" ` +
                    `FOREIGN KEY ("${foreignKey.columnNames.join("\", \"")}") ` +
                    `REFERENCES "${foreignKey.referencedTableName}"("${foreignKey.referencedColumnNames.join("\", \"")}")`;
                if (foreignKey.onDelete)
                    sql += " ON DELETE " + foreignKey.onDelete;
                return this.query(sql);
            });
            yield Promise.all(promises);
        });
    }
    /**
     * Drops a foreign keys from the table.
     */
    dropForeignKeys(tableSchema, foreignKeys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const promises = foreignKeys.map(foreignKey => {
                const sql = `ALTER TABLE "${tableSchema.name}" DROP CONSTRAINT "${foreignKey.name}"`;
                return this.query(sql);
            });
            yield Promise.all(promises);
        });
    }
    /**
     * Creates a new index.
     */
    createIndex(index) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const columnNames = index.columnNames.map(columnName => `"${columnName}"`).join(",");
            const sql = `CREATE ${index.isUnique ? "UNIQUE " : ""}INDEX "${index.name}" ON "${index.tableName}"(${columnNames})`;
            yield this.query(sql);
        });
    }
    /**
     * Drops an index from the table.
     */
    dropIndex(tableName, indexName, isGenerated = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (isGenerated) {
                yield this.query(`ALTER SEQUENCE "${tableName}_id_seq" OWNED BY NONE`);
            }
            const sql = `ALTER TABLE "${tableName}" DROP CONSTRAINT "${indexName}"`; // todo: make sure DROP INDEX should not be used here
            yield this.query(sql);
        });
    }
    /**
     * Creates a database type from a given column metadata.
     */
    normalizeType(column) {
        switch (column.normalizedDataType) {
            case "string":
                return "character varying(" + (column.length ? column.length : 255) + ")";
            case "text":
                return "text";
            case "boolean":
                return "boolean";
            case "integer":
            case "int":
                return "integer";
            case "smallint":
                return "smallint";
            case "bigint":
                return "bigint";
            case "float":
                return "real";
            case "double":
            case "number":
                return "double precision";
            case "decimal":
                if (column.precision && column.scale) {
                    return `decimal(${column.precision},${column.scale})`;
                }
                else if (column.scale) {
                    return `decimal(${column.scale})`;
                }
                else if (column.precision) {
                    return `decimal(${column.precision})`;
                }
                else {
                    return "decimal";
                }
            case "date":
                return "date";
            case "time":
                if (column.timezone) {
                    return "time with time zone";
                }
                else {
                    return "time without time zone";
                }
            case "datetime":
                if (column.timezone) {
                    return "timestamp with time zone";
                }
                else {
                    return "timestamp without time zone";
                }
            case "json":
                return "json";
            case "simple_array":
                return column.length ? "character varying(" + column.length + ")" : "text";
        }
        throw new DataTypeNotSupportedByDriverError_1.DataTypeNotSupportedByDriverError(column.type, "Postgres");
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Database name shortcut.
     */
    get dbName() {
        return this.driver.options.database;
    }
    /**
     * Parametrizes given object of values. Used to create column=value queries.
     */
    parametrize(objectLiteral, startIndex = 0) {
        return Object.keys(objectLiteral).map((key, index) => this.driver.escapeColumnName(key) + "=$" + (startIndex + index + 1));
    }
    /**
     * Builds a query for create column.
     */
    buildCreateColumnSql(column, skipPrimary) {
        let c = "\"" + column.name + "\"";
        if (column.isGenerated === true)
            c += " SERIAL";
        if (!column.isGenerated)
            c += " " + column.type;
        if (column.isNullable !== true)
            c += " NOT NULL";
        if (column.isGenerated)
            c += " PRIMARY KEY";
        return c;
    }
}
exports.PostgresQueryRunner = PostgresQueryRunner;

//# sourceMappingURL=PostgresQueryRunner.js.map
