"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const ConnectionIsNotSetError_1 = require("../error/ConnectionIsNotSetError");
const DriverPackageNotInstalledError_1 = require("../error/DriverPackageNotInstalledError");
const DriverPackageLoadError_1 = require("../error/DriverPackageLoadError");
const DriverUtils_1 = require("../DriverUtils");
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const ColumnMetadata_1 = require("../../metadata/ColumnMetadata");
const moment = require("moment");
const PostgresQueryRunner_1 = require("./PostgresQueryRunner");
const DriverOptionNotSetError_1 = require("../error/DriverOptionNotSetError");
// todo(tests):
// check connection with url
// check if any of required option is not set exception to be thrown
//
/**
 * Organizes communication with PostgreSQL DBMS.
 */
class PostgresDriver {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(connectionOptions, logger, postgres) {
        /**
         * Pool of database connections.
         */
        this.databaseConnectionPool = [];
        this.options = DriverUtils_1.DriverUtils.buildDriverOptions(connectionOptions);
        this.logger = logger;
        this.postgres = postgres;
        // validate options to make sure everything is set
        if (!this.options.host)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("host");
        if (!this.options.username)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("username");
        if (!this.options.database)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("database");
        // if postgres package instance was not set explicitly then try to load it
        if (!postgres)
            this.loadDependencies();
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Performs connection to the database.
     * Based on pooling options, it can either create connection immediately,
     * either create a pool and create connection when needed.
     */
    connect() {
        // build connection options for the driver
        const options = Object.assign({}, {
            host: this.options.host,
            user: this.options.username,
            password: this.options.password,
            database: this.options.database,
            port: this.options.port
        }, this.options.extra || {});
        // pooling is enabled either when its set explicitly to true,
        // either when its not defined at all (e.g. enabled by default)
        if (this.options.usePool === undefined || this.options.usePool === true) {
            this.pool = new this.postgres.Pool(options);
            return Promise.resolve();
        }
        else {
            return new Promise((ok, fail) => {
                this.databaseConnection = {
                    id: 1,
                    connection: new this.postgres.Client(options),
                    isTransactionActive: false
                };
                this.databaseConnection.connection.connect((err) => err ? fail(err) : ok());
            });
        }
    }
    /**
     * Closes connection with database.
     */
    disconnect() {
        if (!this.databaseConnection && !this.pool)
            throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("postgres");
        return new Promise((ok, fail) => {
            const handler = (err) => err ? fail(err) : ok();
            if (this.databaseConnection) {
                this.databaseConnection.connection.end(); // todo: check if it can emit errors
                this.databaseConnection = undefined;
            }
            if (this.databaseConnectionPool) {
                this.databaseConnectionPool.forEach(dbConnection => {
                    if (dbConnection && dbConnection.releaseCallback) {
                        dbConnection.releaseCallback();
                    }
                });
                this.pool.end(handler);
                this.pool = undefined;
                this.databaseConnectionPool = [];
            }
            ok();
        });
    }
    /**
     * Creates a query runner used for common queries.
     */
    createQueryRunner() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.databaseConnection && !this.pool)
                return Promise.reject(new ConnectionIsNotSetError_1.ConnectionIsNotSetError("postgres"));
            const databaseConnection = yield this.retrieveDatabaseConnection();
            return new PostgresQueryRunner_1.PostgresQueryRunner(databaseConnection, this, this.logger);
        });
    }
    /**
     * Access to the native implementation of the database.
     */
    nativeInterface() {
        return {
            driver: this.postgres,
            connection: this.databaseConnection ? this.databaseConnection.connection : undefined,
            pool: this.pool
        };
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type and metadata.
     */
    preparePersistentValue(value, column) {
        switch (column.type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value === true ? 1 : 0;
            case ColumnTypes_1.ColumnTypes.DATE:
                return moment(value).format("YYYY-MM-DD");
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value).format("HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.DATETIME:
                return moment(value).format("YYYY-MM-DD HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.JSON:
                // pg(pg-types) have done JSON.parse conversion
                // https://github.com/brianc/node-pg-types/blob/ed2d0e36e33217b34530727a98d20b325389e73a/lib/textParsers.js#L170
                return value;
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value
                    .map(i => String(i))
                    .join(",");
        }
        return value;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type or metadata.
     */
    prepareHydratedValue(value, columnOrColumnType) {
        const type = columnOrColumnType instanceof ColumnMetadata_1.ColumnMetadata ? columnOrColumnType.type : columnOrColumnType;
        switch (type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value ? true : false;
            case ColumnTypes_1.ColumnTypes.DATE:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD").toDate();
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value, "HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.DATETIME:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.JSON:
                if (typeof value === "string" || !value) {
                    return JSON.parse(value);
                }
                else {
                    return value;
                }
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value.split(",");
        }
        return value;
    }
    /**
     * Replaces parameters in the given sql with special escaping character
     * and an array of parameter names to be passed to a query.
     */
    escapeQueryWithParameters(sql, parameters) {
        if (!parameters || !Object.keys(parameters).length)
            return [sql, []];
        const builtParameters = [];
        const keys = Object.keys(parameters).map(parameter => "(:" + parameter + "\\b)").join("|");
        sql = sql.replace(new RegExp(keys, "g"), (key) => {
            const value = parameters[key.substr(1)];
            if (value instanceof Array) {
                return value.map((v) => {
                    builtParameters.push(v);
                    return "$" + builtParameters.length;
                }).join(", ");
            }
            else {
                builtParameters.push(value);
            }
            return "$" + builtParameters.length;
        }); // todo: make replace only in value statements, otherwise problems
        return [sql, builtParameters];
    }
    /**
     * Escapes a column name.
     */
    escapeColumnName(columnName) {
        return "\"" + columnName + "\"";
    }
    /**
     * Escapes an alias.
     */
    escapeAliasName(aliasName) {
        return "\"" + aliasName + "\"";
    }
    /**
     * Escapes a table name.
     */
    escapeTableName(tableName) {
        return "\"" + tableName + "\"";
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Retrieves a new database connection.
     * If pooling is enabled then connection from the pool will be retrieved.
     * Otherwise active connection will be returned.
     */
    retrieveDatabaseConnection() {
        if (this.pool) {
            return new Promise((ok, fail) => {
                this.pool.connect((err, connection, release) => {
                    if (err) {
                        fail(err);
                        return;
                    }
                    let dbConnection = this.databaseConnectionPool.find(dbConnection => dbConnection.connection === connection);
                    if (!dbConnection) {
                        dbConnection = {
                            id: this.databaseConnectionPool.length,
                            connection: connection,
                            isTransactionActive: false
                        };
                        this.databaseConnectionPool.push(dbConnection);
                    }
                    dbConnection.releaseCallback = () => {
                        if (dbConnection) {
                            this.databaseConnectionPool.splice(this.databaseConnectionPool.indexOf(dbConnection), 1);
                        }
                        release();
                        return Promise.resolve();
                    };
                    ok(dbConnection);
                });
            });
        }
        if (this.databaseConnection)
            return Promise.resolve(this.databaseConnection);
        throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("postgres");
    }
    /**
     * If driver dependency is not given explicitly, then try to load it via "require".
     */
    loadDependencies() {
        if (!require)
            throw new DriverPackageLoadError_1.DriverPackageLoadError();
        try {
            this.postgres = require("pg");
        }
        catch (e) {
            throw new DriverPackageNotInstalledError_1.DriverPackageNotInstalledError("Postgres", "pg");
        }
    }
}
exports.PostgresDriver = PostgresDriver;

//# sourceMappingURL=PostgresDriver.js.map
