"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const TransactionAlreadyStartedError_1 = require("../error/TransactionAlreadyStartedError");
const TransactionNotStartedError_1 = require("../error/TransactionNotStartedError");
const DataTypeNotSupportedByDriverError_1 = require("../error/DataTypeNotSupportedByDriverError");
const ColumnSchema_1 = require("../../schema-builder/schema/ColumnSchema");
const TableSchema_1 = require("../../schema-builder/schema/TableSchema");
const ForeignKeySchema_1 = require("../../schema-builder/schema/ForeignKeySchema");
const PrimaryKeySchema_1 = require("../../schema-builder/schema/PrimaryKeySchema");
const QueryRunnerAlreadyReleasedError_1 = require("../../query-runner/error/QueryRunnerAlreadyReleasedError");
/**
 * Runs queries on a single mysql database connection.
 */
class OracleQueryRunner {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(databaseConnection, driver, logger) {
        this.databaseConnection = databaseConnection;
        this.driver = driver;
        this.logger = logger;
        // -------------------------------------------------------------------------
        // Protected Properties
        // -------------------------------------------------------------------------
        /**
         * Indicates if connection for this query runner is released.
         * Once its released, query runner cannot run queries anymore.
         */
        this.isReleased = false;
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Releases database connection. This is needed when using connection pooling.
     * If connection is not from a pool, it should not be released.
     * You cannot use this class's methods after its released.
     */
    release() {
        if (this.databaseConnection.releaseCallback) {
            this.isReleased = true;
            return this.databaseConnection.releaseCallback();
        }
        return Promise.resolve();
    }
    /**
     * Removes all tables from the currently connected database.
     */
    clearDatabase() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const disableForeignKeysCheckQuery = `SET FOREIGN_KEY_CHECKS = 0;`;
            const dropTablesQuery = `SELECT concat('DROP TABLE IF EXISTS ', table_name, ';') AS query FROM information_schema.tables WHERE table_schema = '${this.dbName}'`;
            const enableForeignKeysCheckQuery = `SET FOREIGN_KEY_CHECKS = 1;`;
            yield this.query(disableForeignKeysCheckQuery);
            const dropQueries = yield this.query(dropTablesQuery);
            yield Promise.all(dropQueries.map(query => this.query(query["query"])));
            yield this.query(enableForeignKeysCheckQuery);
        });
    }
    /**
     * Starts transaction.
     */
    beginTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (this.databaseConnection.isTransactionActive)
                throw new TransactionAlreadyStartedError_1.TransactionAlreadyStartedError();
            // await this.query("START TRANSACTION");
            this.databaseConnection.isTransactionActive = true;
        });
    }
    /**
     * Commits transaction.
     */
    commitTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!this.databaseConnection.isTransactionActive)
                throw new TransactionNotStartedError_1.TransactionNotStartedError();
            yield this.query("COMMIT");
            this.databaseConnection.isTransactionActive = false;
        });
    }
    /**
     * Rollbacks transaction.
     */
    rollbackTransaction() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            if (!this.databaseConnection.isTransactionActive)
                throw new TransactionNotStartedError_1.TransactionNotStartedError();
            yield this.query("ROLLBACK");
            this.databaseConnection.isTransactionActive = false;
        });
    }
    /**
     * Checks if transaction is in progress.
     */
    isTransactionActive() {
        return this.databaseConnection.isTransactionActive;
    }
    /**
     * Executes a given SQL query.
     */
    query(query, parameters) {
        if (this.isReleased)
            throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
        this.logger.logQuery(query, parameters);
        return new Promise((ok, fail) => {
            const handler = (err, result) => {
                if (err) {
                    this.logger.logFailedQuery(query, parameters);
                    this.logger.logQueryError(err);
                    return fail(err);
                }
                ok(result.rows || result.outBinds);
            };
            const executionOptions = {
                autoCommit: this.databaseConnection.isTransactionActive ? false : true
            };
            this.databaseConnection.connection.execute(query, parameters || {}, executionOptions, handler);
        });
    }
    /**
     * Insert a new row with given values into given table.
     */
    insert(tableName, keyValues, generatedColumn) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const keys = Object.keys(keyValues);
            const columns = keys.map(key => this.driver.escapeColumnName(key)).join(", ");
            const values = keys.map(key => ":" + key).join(", ");
            const parameters = keys.map(key => keyValues[key]);
            const insertSql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(${columns}) VALUES (${values})`;
            if (generatedColumn) {
                const sql2 = `declare lastId number; begin ${insertSql} returning "id" into lastId; dbms_output.enable; dbms_output.put_line(lastId); dbms_output.get_line(:ln, :st); end;`;
                const saveResult = yield this.query(sql2, parameters.concat([
                    { dir: this.driver.oracle.BIND_OUT, type: this.driver.oracle.STRING, maxSize: 32767 },
                    { dir: this.driver.oracle.BIND_OUT, type: this.driver.oracle.NUMBER }
                ]));
                return parseInt(saveResult[0]);
            }
            else {
                return this.query(insertSql, parameters);
            }
        });
    }
    /**
     * Updates rows that match given conditions in the given table.
     */
    update(tableName, valuesMap, conditions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const updateValues = this.parametrize(valuesMap).join(", ");
            const conditionString = this.parametrize(conditions).join(" AND ");
            const sql = `UPDATE ${this.driver.escapeTableName(tableName)} SET ${updateValues} ${conditionString ? (" WHERE " + conditionString) : ""}`;
            const conditionParams = Object.keys(conditions).map(key => conditions[key]);
            const updateParams = Object.keys(valuesMap).map(key => valuesMap[key]);
            const allParameters = updateParams.concat(conditionParams);
            yield this.query(sql, allParameters);
        });
    }
    /**
     * Deletes from the given table by a given conditions.
     */
    delete(tableName, conditions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const conditionString = this.parametrize(conditions).join(" AND ");
            const sql = `DELETE FROM ${this.driver.escapeTableName(tableName)} WHERE ${conditionString}`;
            const parameters = Object.keys(conditions).map(key => conditions[key]);
            yield this.query(sql, parameters);
        });
    }
    /**
     * Inserts rows into the closure table.
     */
    insertIntoClosureTable(tableName, newEntityId, parentId, hasLevel) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            let sql = "";
            if (hasLevel) {
                sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(ancestor, descendant, level) ` +
                    `SELECT ancestor, ${newEntityId}, level + 1 FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId} ` +
                    `UNION ALL SELECT ${newEntityId}, ${newEntityId}, 1`;
            }
            else {
                sql = `INSERT INTO ${this.driver.escapeTableName(tableName)}(ancestor, descendant) ` +
                    `SELECT ancestor, ${newEntityId} FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId} ` +
                    `UNION ALL SELECT ${newEntityId}, ${newEntityId}`;
            }
            yield this.query(sql);
            const results = yield this.query(`SELECT MAX(level) as level FROM ${this.driver.escapeTableName(tableName)} WHERE descendant = ${parentId}`);
            return results && results[0] && results[0]["level"] ? parseInt(results[0]["level"]) + 1 : 1;
        });
    }
    /**
     * Loads all tables (with given names) from the database and creates a TableSchema from them.
     */
    loadSchemaTables(tableNames, namingStrategy) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            // if no tables given then no need to proceed
            if (!tableNames || !tableNames.length)
                return [];
            // load tables, columns, indices and foreign keys
            const tableNamesString = tableNames.map(name => "'" + name + "'").join(", ");
            const tablesSql = `SELECT TABLE_NAME FROM user_tables WHERE TABLE_NAME IN (${tableNamesString})`;
            const columnsSql = `SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, NULLABLE, IDENTITY_COLUMN FROM all_tab_cols WHERE TABLE_NAME IN (${tableNamesString})`;
            const indicesSql = `SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA = '${this.dbName}' AND INDEX_NAME != 'PRIMARY'`;
            const foreignKeysSql = `SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = '${this.dbName}' AND REFERENCED_COLUMN_NAME IS NOT NULL`;
            const uniqueKeysSql = `SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_SCHEMA = '${this.dbName}' AND CONSTRAINT_TYPE = 'UNIQUE'`;
            const constraintsSql = `SELECT cols.table_name, cols.column_name, cols.position, cons.constraint_type, cons.constraint_name
FROM all_constraints cons, all_cons_columns cols WHERE cols.table_name IN (${tableNamesString}) 
AND cons.constraint_name = cols.constraint_name AND cons.owner = cols.owner ORDER BY cols.table_name, cols.position`;
            const [dbTables, dbColumns, /*dbIndices, dbForeignKeys, dbUniqueKeys, */ constraints] = yield Promise.all([
                this.query(tablesSql),
                this.query(columnsSql),
                // this.query(indicesSql),
                // this.query(foreignKeysSql),
                // this.query(uniqueKeysSql),
                this.query(constraintsSql),
            ]);
            // if tables were not found in the db, no need to proceed
            if (!dbTables.length)
                return [];
            // create table schemas for loaded tables
            return dbTables.map(dbTable => {
                const tableSchema = new TableSchema_1.TableSchema(dbTable["TABLE_NAME"]);
                // create column schemas from the loaded columns
                tableSchema.columns = dbColumns
                    .filter(dbColumn => dbColumn["TABLE_NAME"] === tableSchema.name)
                    .map(dbColumn => {
                    const isPrimary = !!constraints
                        .find(constraint => {
                        return constraint["TABLE_NAME"] === tableSchema.name &&
                            constraint["CONSTRAINT_TYPE"] === "P" &&
                            constraint["COLUMN_NAME"] === dbColumn["COLUMN_NAME"];
                    });
                    let columnType = dbColumn["DATA_TYPE"].toLowerCase();
                    if (dbColumn["DATA_TYPE"].toLowerCase() === "varchar2" && dbColumn["DATA_LENGTH"] !== null) {
                        columnType += "(" + dbColumn["DATA_LENGTH"] + ")";
                    }
                    else if (dbColumn["DATA_PRECISION"] !== null && dbColumn["DATA_SCALE"] !== null) {
                        columnType += "(" + dbColumn["DATA_PRECISION"] + "," + dbColumn["DATA_SCALE"] + ")";
                    }
                    else if (dbColumn["DATA_SCALE"] !== null) {
                        columnType += "(0," + dbColumn["DATA_SCALE"] + ")";
                    }
                    else if (dbColumn["DATA_PRECISION"] !== null) {
                        columnType += "(" + dbColumn["DATA_PRECISION"] + ")";
                    }
                    const columnSchema = new ColumnSchema_1.ColumnSchema();
                    columnSchema.name = dbColumn["COLUMN_NAME"];
                    columnSchema.type = columnType;
                    columnSchema.default = dbColumn["COLUMN_DEFAULT"] !== null && dbColumn["COLUMN_DEFAULT"] !== undefined ? dbColumn["COLUMN_DEFAULT"] : undefined;
                    columnSchema.isNullable = dbColumn["NULLABLE"] !== "N";
                    columnSchema.isPrimary = isPrimary;
                    columnSchema.isGenerated = dbColumn["IDENTITY_COLUMN"] === "YES"; // todo
                    columnSchema.comment = ""; // todo
                    return columnSchema;
                });
                // create primary key schema
                tableSchema.primaryKeys = constraints
                    .filter(constraint => constraint["TABLE_NAME"] === tableSchema.name && constraint["CONSTRAINT_TYPE"] === "P")
                    .map(constraint => new PrimaryKeySchema_1.PrimaryKeySchema(constraint["CONSTRAINT_NAME"], constraint["COLUMN_NAME"]));
                // create foreign key schemas from the loaded indices
                tableSchema.foreignKeys = constraints
                    .filter(constraint => constraint["TABLE_NAME"] === tableSchema.name && constraint["CONSTRAINT_TYPE"] === "R")
                    .map(constraint => new ForeignKeySchema_1.ForeignKeySchema(constraint["CONSTRAINT_NAME"], [], [], "", "")); // todo: fix missing params
                console.log(tableSchema);
                // create index schemas from the loaded indices
                // tableSchema.indices = dbIndices
                //     .filter(dbIndex => {
                //         return  dbIndex["table_name"] === tableSchema.name &&
                //             (!tableSchema.foreignKeys.find(foreignKey => foreignKey.name === dbIndex["INDEX_NAME"])) &&
                //             (!tableSchema.primaryKeys.find(primaryKey => primaryKey.name === dbIndex["INDEX_NAME"]));
                //     })
                //     .map(dbIndex => dbIndex["INDEX_NAME"])
                //     .filter((value, index, self) => self.indexOf(value) === index) // unqiue
                //     .map(dbIndexName => {
                //         const columnNames = dbIndices
                //             .filter(dbIndex => dbIndex["TABLE_NAME"] === tableSchema.name && dbIndex["INDEX_NAME"] === dbIndexName)
                //             .map(dbIndex => dbIndex["COLUMN_NAME"]);
                //
                //         return new IndexSchema(dbTable["TABLE_NAME"], dbIndexName, columnNames, false /* todo: uniqueness */);
                //     });
                return tableSchema;
            });
        });
    }
    /**
     * Creates a new table from the given table metadata and column metadatas.
     */
    createTable(table) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const columnDefinitions = table.columns.map(column => this.buildCreateColumnSql(column)).join(", ");
            let sql = `CREATE TABLE "${table.name}" (${columnDefinitions}`;
            const primaryKeyColumns = table.columns.filter(column => column.isPrimary);
            if (primaryKeyColumns.length > 0)
                sql += `, PRIMARY KEY(${primaryKeyColumns.map(column => `"${column.name}"`).join(", ")})`;
            sql += `)`;
            yield this.query(sql);
        });
    }
    /**
     * Creates a new column from the column metadata in the table.
     */
    createColumns(tableSchema, columns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const queries = columns.map(column => {
                const sql = `ALTER TABLE "${tableSchema.name}" ADD ${this.buildCreateColumnSql(column)}`;
                return this.query(sql);
            });
            yield Promise.all(queries);
        });
    }
    /**
     * Changes a column in the table.
     */
    changeColumns(tableSchema, changedColumns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const updatePromises = changedColumns.map((changedColumn) => __awaiter(this, void 0, void 0, function* () {
                if (changedColumn.newColumn.isGenerated !== changedColumn.oldColumn.isGenerated) {
                    if (changedColumn.newColumn.isGenerated) {
                        if (tableSchema.primaryKeys.length > 0 && changedColumn.oldColumn.isPrimary) {
                            console.log(tableSchema.primaryKeys);
                            const dropPrimarySql = `ALTER TABLE "${tableSchema.name}" DROP CONSTRAINT "${tableSchema.primaryKeys[0].name}"`;
                            yield this.query(dropPrimarySql);
                        }
                        // since modifying identity column is not supported yet, we need to recreate this column
                        const dropSql = `ALTER TABLE "${tableSchema.name}" DROP COLUMN "${changedColumn.newColumn.name}"`;
                        yield this.query(dropSql);
                        const createSql = `ALTER TABLE "${tableSchema.name}" ADD ${this.buildCreateColumnSql(changedColumn.newColumn)}`;
                        yield this.query(createSql);
                    }
                    else {
                        const sql = `ALTER TABLE "${tableSchema.name}" MODIFY "${changedColumn.newColumn.name}" DROP IDENTITY`;
                        yield this.query(sql);
                    }
                }
                if (changedColumn.newColumn.isNullable !== changedColumn.oldColumn.isNullable) {
                    const sql = `ALTER TABLE "${tableSchema.name}" MODIFY "${changedColumn.newColumn.name}" ${changedColumn.newColumn.type} ${changedColumn.newColumn.isNullable ? "NULL" : "NOT NULL"}`;
                    yield this.query(sql);
                }
                else if (changedColumn.newColumn.type !== changedColumn.oldColumn.type) {
                    const sql = `ALTER TABLE "${tableSchema.name}" MODIFY "${changedColumn.newColumn.name}" ${changedColumn.newColumn.type}`;
                    yield this.query(sql);
                }
            }));
            yield Promise.all(updatePromises);
        });
    }
    /**
     * Drops the columns in the table.
     */
    dropColumns(dbTable, columns) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const dropPromises = columns.map(column => {
                return this.query(`ALTER TABLE "${dbTable.name}" DROP COLUMN "${column.name}"`);
            });
            yield Promise.all(dropPromises);
        });
    }
    /**
     * Updates table's primary keys.
     */
    updatePrimaryKeys(dbTable) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const primaryColumnNames = dbTable.primaryKeys.map(primaryKey => "\"" + primaryKey.columnName + "\"");
            console.log(dbTable.primaryKeys);
            if (dbTable.primaryKeys.length > 0 && dbTable.primaryKeys[0].name)
                yield this.query(`ALTER TABLE "${dbTable.name}" DROP CONSTRAINT "${dbTable.primaryKeys[0].name}"`);
            if (primaryColumnNames.length > 0)
                yield this.query(`ALTER TABLE "${dbTable.name}" ADD PRIMARY KEY (${primaryColumnNames.join(", ")})`);
        });
    }
    /**
     * Creates a new foreign keys.
     */
    createForeignKeys(dbTable, foreignKeys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const promises = foreignKeys.map(foreignKey => {
                const columnNames = foreignKey.columnNames.map(column => "\"" + column + "\"").join(", ");
                const referencedColumnNames = foreignKey.referencedColumnNames.map(column => "\"" + column + "\"").join(",");
                let sql = `ALTER TABLE "${dbTable.name}" ADD CONSTRAINT "${foreignKey.name}" ` +
                    `FOREIGN KEY (${columnNames}) ` +
                    `REFERENCES "${foreignKey.referencedTableName}"(${referencedColumnNames})`;
                if (foreignKey.onDelete)
                    sql += " ON DELETE " + foreignKey.onDelete;
                return this.query(sql);
            });
            yield Promise.all(promises);
        });
    }
    /**
     * Drops a foreign keys from the table.
     */
    dropForeignKeys(tableSchema, foreignKeys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const promises = foreignKeys.map(foreignKey => {
                const sql = `ALTER TABLE "${tableSchema.name}" DROP CONSTRAINT "${foreignKey.name}"`;
                return this.query(sql);
            });
            yield Promise.all(promises);
        });
    }
    /**
     * Creates a new index.
     */
    createIndex(index) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const columns = index.columnNames.map(columnName => "\"" + columnName + "\"").join(", ");
            const sql = `CREATE ${index.isUnique ? "UNIQUE" : ""} INDEX "${index.name}" ON "${index.tableName}"(${columns})`;
            yield this.query(sql);
        });
    }
    /**
     * Drops an index from the table.
     */
    dropIndex(tableName, indexName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isReleased)
                throw new QueryRunnerAlreadyReleasedError_1.QueryRunnerAlreadyReleasedError();
            const sql = `ALTER TABLE "${tableName}" DROP INDEX "${indexName}"`;
            yield this.query(sql);
        });
    }
    /**
     * Creates a database type from a given column metadata.
     */
    normalizeType(column) {
        switch (column.normalizedDataType) {
            case "string":
                return "varchar2(" + (column.length ? column.length : 255) + ")";
            case "text":
                return "clob";
            case "boolean":
                return "number(1)";
            case "integer":
            case "int":
                // if (column.isGenerated)
                //     return `number(22)`;
                if (column.precision && column.scale)
                    return `number(${column.precision},${column.scale})`;
                if (column.precision)
                    return `number(${column.precision},0)`;
                if (column.scale)
                    return `number(0,${column.scale})`;
                return "number(10,0)";
            case "smallint":
                return "number(5)";
            case "bigint":
                return "number(20)";
            case "float":
                if (column.precision && column.scale)
                    return `float(${column.precision},${column.scale})`;
                if (column.precision)
                    return `float(${column.precision},0)`;
                if (column.scale)
                    return `float(0,${column.scale})`;
                return `float(126)`;
            case "double":
            case "number":
                return "float(126)";
            case "decimal":
                if (column.precision && column.scale) {
                    return `decimal(${column.precision},${column.scale})`;
                }
                else if (column.scale) {
                    return `decimal(0,${column.scale})`;
                }
                else if (column.precision) {
                    return `decimal(${column.precision})`;
                }
                else {
                    return "decimal";
                }
            case "date":
                return "date";
            case "time":
                return "date";
            case "datetime":
                return "timestamp(0)";
            case "json":
                return "clob";
            case "simple_array":
                return column.length ? "varchar2(" + column.length + ")" : "text";
        }
        throw new DataTypeNotSupportedByDriverError_1.DataTypeNotSupportedByDriverError(column.type, "Oracle");
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Database name shortcut.
     */
    get dbName() {
        return this.driver.options.database;
    }
    /**
     * Parametrizes given object of values. Used to create column=value queries.
     */
    parametrize(objectLiteral) {
        return Object.keys(objectLiteral).map(key => this.driver.escapeColumnName(key) + "=:" + key);
    }
    /**
     * Builds a query for create column.
     */
    buildCreateColumnSql(column) {
        let c = `"${column.name}" ` + column.type;
        if (column.isNullable !== true && !column.isGenerated)
            c += " NOT NULL";
        // if (column.isPrimary === true && addPrimary)
        //     c += " PRIMARY KEY";
        if (column.isGenerated === true)
            c += " GENERATED BY DEFAULT ON NULL AS IDENTITY";
        // if (column.comment) // todo: less priority, fix it later
        //     c += " COMMENT '" + column.comment + "'";
        return c;
    }
}
exports.OracleQueryRunner = OracleQueryRunner;

//# sourceMappingURL=OracleQueryRunner.js.map
