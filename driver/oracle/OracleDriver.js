"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const ConnectionIsNotSetError_1 = require("../error/ConnectionIsNotSetError");
const DriverPackageNotInstalledError_1 = require("../error/DriverPackageNotInstalledError");
const DriverPackageLoadError_1 = require("../error/DriverPackageLoadError");
const DriverUtils_1 = require("../DriverUtils");
const OracleQueryRunner_1 = require("./OracleQueryRunner");
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const moment = require("moment");
const ColumnMetadata_1 = require("../../metadata/ColumnMetadata");
const DriverOptionNotSetError_1 = require("../error/DriverOptionNotSetError");
/**
 * Organizes communication with Oracle DBMS.
 */
class OracleDriver {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(options, logger, oracle) {
        /**
         * Pool of database connections.
         */
        this.databaseConnectionPool = [];
        this.options = DriverUtils_1.DriverUtils.buildDriverOptions(options, { useSid: true });
        this.logger = logger;
        this.oracle = oracle;
        // validate options to make sure everything is set
        if (!this.options.host)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("host");
        if (!this.options.username)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("username");
        if (!this.options.sid)
            throw new DriverOptionNotSetError_1.DriverOptionNotSetError("sid");
        // if oracle package instance was not set explicitly then try to load it
        if (!oracle)
            this.loadDependencies();
        this.oracle.outFormat = this.oracle.OBJECT;
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Performs connection to the database.
     * Based on pooling options, it can either create connection immediately,
     * either create a pool and create connection when needed.
     */
    connect() {
        // build connection options for the driver
        const options = Object.assign({}, {
            user: this.options.username,
            password: this.options.password,
            connectString: this.options.host + ":" + this.options.port + "/" + this.options.sid,
        }, this.options.extra || {});
        // pooling is enabled either when its set explicitly to true,
        // either when its not defined at all (e.g. enabled by default)
        if (this.options.usePool === undefined || this.options.usePool === true) {
            return new Promise((ok, fail) => {
                this.oracle.createPool(options, (err, pool) => {
                    if (err)
                        return fail(err);
                    this.pool = pool;
                    ok();
                });
            });
        }
        else {
            return new Promise((ok, fail) => {
                this.oracle.getConnection(options, (err, connection) => {
                    if (err)
                        return fail(err);
                    this.databaseConnection = {
                        id: 1,
                        connection: connection,
                        isTransactionActive: false
                    };
                    this.databaseConnection.connection.connect((err) => err ? fail(err) : ok());
                });
            });
        }
    }
    /**
     * Closes connection with the database.
     */
    disconnect() {
        if (!this.databaseConnection && !this.pool)
            throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("oracle");
        return new Promise((ok, fail) => {
            const handler = (err) => err ? fail(err) : ok();
            // if pooling is used, then disconnect from it
            if (this.pool) {
                this.pool.close(handler);
                this.pool = undefined;
                this.databaseConnectionPool = [];
            }
            // if single connection is opened, then close it
            if (this.databaseConnection) {
                this.databaseConnection.connection.close(handler);
                this.databaseConnection = undefined;
            }
        });
    }
    /**
     * Creates a query runner used for common queries.
     */
    createQueryRunner() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.databaseConnection && !this.pool)
                return Promise.reject(new ConnectionIsNotSetError_1.ConnectionIsNotSetError("oracle"));
            const databaseConnection = yield this.retrieveDatabaseConnection();
            return new OracleQueryRunner_1.OracleQueryRunner(databaseConnection, this, this.logger);
        });
    }
    /**
     * Access to the native implementation of the database.
     */
    nativeInterface() {
        return {
            driver: this.oracle,
            connection: this.databaseConnection ? this.databaseConnection.connection : undefined,
            pool: this.pool
        };
    }
    /**
     * Replaces parameters in the given sql with special escaping character
     * and an array of parameter names to be passed to a query.
     */
    escapeQueryWithParameters(sql, parameters) {
        if (!parameters || !Object.keys(parameters).length)
            return [sql, []];
        const escapedParameters = [];
        const keys = Object.keys(parameters).map(parameter => "(:" + parameter + "\\b)").join("|");
        sql = sql.replace(new RegExp(keys, "g"), (key) => {
            escapedParameters.push(parameters[key.substr(1)]);
            return ":" + key;
        }); // todo: make replace only in value statements, otherwise problems
        return [sql, escapedParameters];
    }
    /**
     * Escapes a column name.
     */
    escapeColumnName(columnName) {
        return `"${columnName}"`; // "`" + columnName + "`";
    }
    /**
     * Escapes an alias.
     */
    escapeAliasName(aliasName) {
        return `"${aliasName}"`;
    }
    /**
     * Escapes a table name.
     */
    escapeTableName(tableName) {
        return `"${tableName}"`;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type and metadata.
     */
    preparePersistentValue(value, column) {
        switch (column.type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value === true ? 1 : 0;
            case ColumnTypes_1.ColumnTypes.DATE:
                return moment(value).format("YYYY-MM-DD");
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value).format("HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.DATETIME:
                return moment(value).format("YYYY-MM-DD HH:mm:ss");
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.stringify(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value
                    .map(i => String(i))
                    .join(",");
        }
        return value;
    }
    /**
     * Prepares given value to a value to be persisted, based on its column type or metadata.
     */
    prepareHydratedValue(value, columnOrColumnType) {
        const type = columnOrColumnType instanceof ColumnMetadata_1.ColumnMetadata ? columnOrColumnType.type : columnOrColumnType;
        switch (type) {
            case ColumnTypes_1.ColumnTypes.BOOLEAN:
                return value ? true : false;
            case ColumnTypes_1.ColumnTypes.DATE:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD").toDate();
            case ColumnTypes_1.ColumnTypes.TIME:
                return moment(value, "HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.DATETIME:
                if (value instanceof Date)
                    return value;
                return moment(value, "YYYY-MM-DD HH:mm:ss").toDate();
            case ColumnTypes_1.ColumnTypes.JSON:
                return JSON.parse(value);
            case ColumnTypes_1.ColumnTypes.SIMPLE_ARRAY:
                return value.split(",");
        }
        return value;
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Retrieves a new database connection.
     * If pooling is enabled then connection from the pool will be retrieved.
     * Otherwise active connection will be returned.
     */
    retrieveDatabaseConnection() {
        if (this.pool) {
            return new Promise((ok, fail) => {
                this.pool.getConnection((err, connection) => {
                    if (err)
                        return fail(err);
                    let dbConnection = this.databaseConnectionPool.find(dbConnection => dbConnection.connection === connection);
                    if (!dbConnection) {
                        dbConnection = {
                            id: this.databaseConnectionPool.length,
                            connection: connection,
                            isTransactionActive: false
                        };
                        dbConnection.releaseCallback = () => {
                            return new Promise((ok, fail) => {
                                connection.close((err) => {
                                    if (err)
                                        return fail(err);
                                    if (this.pool && dbConnection) {
                                        this.databaseConnectionPool.splice(this.databaseConnectionPool.indexOf(dbConnection), 1);
                                    }
                                    ok();
                                });
                            });
                        };
                        this.databaseConnectionPool.push(dbConnection);
                    }
                    ok(dbConnection);
                });
            });
        }
        if (this.databaseConnection)
            return Promise.resolve(this.databaseConnection);
        throw new ConnectionIsNotSetError_1.ConnectionIsNotSetError("oracle");
    }
    /**
     * If driver dependency is not given explicitly, then try to load it via "require".
     */
    loadDependencies() {
        if (!require)
            throw new DriverPackageLoadError_1.DriverPackageLoadError();
        try {
            this.oracle = require("oracledb");
        }
        catch (e) {
            throw new DriverPackageNotInstalledError_1.DriverPackageNotInstalledError("Oracle", "oracledb");
        }
    }
}
exports.OracleDriver = OracleDriver;

//# sourceMappingURL=OracleDriver.js.map
