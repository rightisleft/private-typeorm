"use strict";
/**
 * Thrown when required driver's package is not installed.
 */
class DriverPackageNotInstalledError extends Error {
    constructor(driverName, packageName) {
        super();
        this.name = "DriverPackageNotInstalledError";
        this.message = `${driverName} package has not been found installed. Try to install it: npm install ${packageName} --save`;
    }
}
exports.DriverPackageNotInstalledError = DriverPackageNotInstalledError;

//# sourceMappingURL=DriverPackageNotInstalledError.js.map
