"use strict";
/**
 * Thrown if database driver does not support pooling.
 */
class DriverPoolingNotSupportedError extends Error {
    constructor(driverName) {
        super();
        this.name = "DriverPoolingNotSupportedError";
        this.message = `Connection pooling is not supported by (${driverName}) driver.`;
    }
}
exports.DriverPoolingNotSupportedError = DriverPoolingNotSupportedError;

//# sourceMappingURL=DriverPoolingNotSupportedError.js.map
