"use strict";
/**
 * Thrown if some data type is not supported by a driver.
 */
class DataTypeNotSupportedByDriverError extends Error {
    constructor(dataType, driverName) {
        super();
        this.name = "DataTypeNotSupportedByDriverError";
        this.message = `Specified type (${dataType}) is not supported by ${driverName} driver.`;
    }
}
exports.DataTypeNotSupportedByDriverError = DataTypeNotSupportedByDriverError;

//# sourceMappingURL=DataTypeNotSupportedByDriverError.js.map
