"use strict";
const MetadataArgsStorage_1 = require("./metadata-args/MetadataArgsStorage");
/**
 * Gets metadata args storage.
 */
function getMetadataArgsStorage() {
    // we should not get MetadataArgsStorage from the consumer's container because it brings too much problems
    // the main problem is that if any entity (or any other) will be imported before consumer will call
    // useContainer method with his own container implementation, that entity will be registered in the
    // old old container (default one post probably) and consumer will his entity.
    // calling useContainer before he imports any entity (or any other) is not always convenient.
    return exports.defaultContainer.get(MetadataArgsStorage_1.MetadataArgsStorage);
}
exports.getMetadataArgsStorage = getMetadataArgsStorage;
/**
 * Container to be used by this library for inversion control. If container was not implicitly set then by default
 * container simply creates a new instance of the given class.
 */
exports.defaultContainer = new (class {
    constructor() {
        this.instances = [];
    }
    get(someClass) {
        let instance = this.instances.find(instance => instance.type === someClass);
        if (!instance) {
            instance = { type: someClass, object: new someClass() };
            this.instances.push(instance);
        }
        return instance.object;
    }
})();
let userContainer;
let userContainerOptions;
/**
 * Sets container to be used by this library.
 */
function useContainer(iocContainer, options) {
    userContainer = iocContainer;
    userContainerOptions = options;
}
exports.useContainer = useContainer;
/**
 * Gets the IOC container used by this library.
 */
function getFromContainer(someClass) {
    if (userContainer) {
        try {
            const instance = userContainer.get(someClass);
            if (instance)
                return instance;
            if (!userContainerOptions || !userContainerOptions.fallback)
                return instance;
        }
        catch (error) {
            if (!userContainerOptions || !userContainerOptions.fallbackOnErrors)
                throw error;
        }
    }
    return exports.defaultContainer.get(someClass);
}
exports.getFromContainer = getFromContainer;

//# sourceMappingURL=container.js.map
