"use strict";
const EntityMetadataNotFound_1 = require("../error/EntityMetadataNotFound");
/**
 * Array for the entity metadatas.
 */
class EntityMetadataCollection extends Array {
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    hasTarget(target) {
        return !!this.find(metadata => metadata.target === target || (typeof target === "string" && metadata.targetName === target));
    }
    findByTarget(target) {
        const metadata = this.find(metadata => metadata.target === target || (typeof target === "string" && metadata.targetName === target));
        if (!metadata)
            throw new EntityMetadataNotFound_1.EntityMetadataNotFound(target);
        return metadata;
    }
    findByName(name) {
        const metadata = this.find(metadata => metadata.name === name);
        if (!metadata)
            throw new EntityMetadataNotFound_1.EntityMetadataNotFound(name);
        return metadata;
    }
    filter(callbackfn, thisArg) {
        thisArg = thisArg || void 0;
        return this.reduce(function (out, val, index, array) {
            if (callbackfn.call(thisArg, val, index, array)) {
                out.push(val);
            }
            return out;
        }, new EntityMetadataCollection());
    }
}
exports.EntityMetadataCollection = EntityMetadataCollection;

//# sourceMappingURL=EntityMetadataCollection.js.map
