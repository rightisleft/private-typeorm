"use strict";
const TargetMetadataArgsCollection_1 = require("./TargetMetadataArgsCollection");
class PropertyMetadataArgsCollection extends TargetMetadataArgsCollection_1.TargetMetadataArgsCollection {
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    filterRepeatedMetadatas(existsMetadatas) {
        return this.filter(metadata => {
            return !existsMetadatas.find(fieldFromDocument => fieldFromDocument.propertyName === metadata.propertyName);
        });
    }
    findByProperty(propertyName) {
        return this.find(item => item.propertyName === propertyName);
    }
    hasWithProperty(propertyName) {
        return !!this.findByProperty(propertyName);
    }
}
exports.PropertyMetadataArgsCollection = PropertyMetadataArgsCollection;

//# sourceMappingURL=PropertyMetadataArgsCollection.js.map
