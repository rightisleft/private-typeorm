"use strict";
/**
 * Contains all information about entity's relation count.
 */
class RelationCountMetadata {
    // ---------------------------------------------------------------------
    // Constructor
    // ---------------------------------------------------------------------
    constructor(args) {
        this.target = args.target;
        this.propertyName = args.propertyName;
        this.relation = args.relation;
    }
}
exports.RelationCountMetadata = RelationCountMetadata;

//# sourceMappingURL=RelationCountMetadata.js.map
