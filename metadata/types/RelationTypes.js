"use strict";
/**
 * Provides a constants for each relation type.
 */
class RelationTypes {
}
exports.RelationTypes = RelationTypes;
RelationTypes.ONE_TO_ONE = "one-to-one";
RelationTypes.ONE_TO_MANY = "one-to-many";
RelationTypes.MANY_TO_ONE = "many-to-one";
RelationTypes.MANY_TO_MANY = "many-to-many";

//# sourceMappingURL=RelationTypes.js.map
