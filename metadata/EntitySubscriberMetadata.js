"use strict";
/**
 * Contains metadata information about ORM event subscribers.
 */
class EntitySubscriberMetadata {
    constructor(args) {
        this.target = args.target;
    }
}
exports.EntitySubscriberMetadata = EntitySubscriberMetadata;

//# sourceMappingURL=EntitySubscriberMetadata.js.map
