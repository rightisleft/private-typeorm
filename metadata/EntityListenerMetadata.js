"use strict";
/**
 * This metadata contains all information about entity's listeners.
 */
class EntityListenerMetadata {
    // ---------------------------------------------------------------------
    // Constructor
    // ---------------------------------------------------------------------
    constructor(args) {
        this.target = args.target;
        this.propertyName = args.propertyName;
        this.type = args.type;
    }
}
exports.EntityListenerMetadata = EntityListenerMetadata;

//# sourceMappingURL=EntityListenerMetadata.js.map
