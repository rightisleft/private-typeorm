#!/usr/bin/env node
"use strict";
const SchemaSyncCommand_1 = require("./commands/SchemaSyncCommand");
const SchemaDropCommand_1 = require("./commands/SchemaDropCommand");
const QueryCommand_1 = require("./commands/QueryCommand");
const EntityGenerateCommand_1 = require("./commands/EntityGenerateCommand");
require("yargonaut")
    .style("blue")
    .style("yellow", "required")
    .helpStyle("green")
    .errorsStyle("red");
require("yargs")
    .usage("Usage: $0 <command> [options]")
    .command(new SchemaSyncCommand_1.SchemaSyncCommand())
    .command(new SchemaDropCommand_1.SchemaDropCommand())
    .command(new QueryCommand_1.QueryCommand())
    .command(new EntityGenerateCommand_1.EntityGenerateCommand())
    .demand(1)
    .version(() => require("./package.json").version)
    .alias("v", "version")
    .help("h")
    .alias("h", "help")
    .argv;

//# sourceMappingURL=cli.js.map
