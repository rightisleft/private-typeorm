"use strict";
/**
 * Thrown when consumer tries to use query runner from query runner provider after it was released.
 */
class QueryRunnerProviderAlreadyReleasedError extends Error {
    constructor() {
        super();
        this.name = "QueryRunnerProviderAlreadyReleasedError";
        this.message = `Database connection provided by a query runner was already released, cannot continue to use its querying methods anymore.`;
        this.stack = new Error().stack;
    }
}
exports.QueryRunnerProviderAlreadyReleasedError = QueryRunnerProviderAlreadyReleasedError;

//# sourceMappingURL=QueryRunnerProviderAlreadyReleasedError.js.map
