"use strict";
const path = require("path");
/**
 * Loads all exported classes from the given directory.
 */
function importClassesFromDirectories(directories, formats = [".js", ".ts"]) {
    function loadFileClasses(exported, allLoaded) {
        if (exported instanceof Function) {
            allLoaded.push(exported);
        }
        else if (exported instanceof Object) {
            Object.keys(exported).forEach(key => loadFileClasses(exported[key], allLoaded));
        }
        else if (exported instanceof Array) {
            exported.forEach((i) => loadFileClasses(i, allLoaded));
        }
        return allLoaded;
    }
    const allFiles = directories.reduce((allDirs, dir) => {
        return allDirs.concat(require("glob").sync(path.normalize(dir)));
    }, []);
    const dirs = allFiles
        .filter(file => {
        const dtsExtension = file.substring(file.length - 5, file.length);
        return formats.indexOf(path.extname(file)) !== -1 && dtsExtension !== ".d.ts";
    })
        .map(file => require(path.resolve(file)));
    return loadFileClasses(dirs, []);
}
exports.importClassesFromDirectories = importClassesFromDirectories;
function importJsonsFromDirectories(directories, format = ".json") {
    const allFiles = directories.reduce((allDirs, dir) => {
        return allDirs.concat(require("glob").sync(path.normalize(dir)));
    }, []);
    return allFiles
        .filter(file => path.extname(file) === format)
        .map(file => require(path.resolve(file)));
}
exports.importJsonsFromDirectories = importJsonsFromDirectories;

//# sourceMappingURL=DirectoryExportedClassesLoader.js.map
