"use strict";
const container_1 = require("../../container");
/**
 * DiscriminatorColumn is a special type column used on entity class (not entity property)
 * and creates a special column which will contain an entity type.
 * This type is required for entities which use single table inheritance pattern.
 */
function DiscriminatorColumn(discriminatorOptions) {
    return function (target) {
        // if column options are not given then create a new empty options
        const options = {
            name: discriminatorOptions.name,
            type: discriminatorOptions.type
        };
        // create and register a new column metadata
        const args = {
            target: target,
            mode: "discriminator",
            propertyName: discriminatorOptions.name,
            options: options
        };
        container_1.getMetadataArgsStorage().columns.add(args);
    };
}
exports.DiscriminatorColumn = DiscriminatorColumn;

//# sourceMappingURL=DiscriminatorColumn.js.map
