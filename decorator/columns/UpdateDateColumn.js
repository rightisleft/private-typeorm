"use strict";
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const container_1 = require("../../container");
/**
 * This column will store an update date of the updated object.
 * This date is being updated each time you persist the object.
 */
function UpdateDateColumn(options) {
    return function (object, propertyName) {
        const reflectedType = ColumnTypes_1.ColumnTypes.typeToString(Reflect.getMetadata("design:type", object, propertyName));
        // if column options are not given then create a new empty options
        if (!options)
            options = {};
        // implicitly set a type, because this column's type cannot be anything else except date
        options = Object.assign({ type: ColumnTypes_1.ColumnTypes.DATETIME }, options);
        // create and register a new column metadata
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            propertyType: reflectedType,
            mode: "updateDate",
            options: options
        };
        container_1.getMetadataArgsStorage().columns.add(args);
    };
}
exports.UpdateDateColumn = UpdateDateColumn;

//# sourceMappingURL=UpdateDateColumn.js.map
