"use strict";
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const container_1 = require("../../container");
/**
 * This column will store a creation date of the inserted object.
 * Creation date is generated and inserted only once,
 * at the first time when you create an object, the value is inserted into the table, and is never touched again.
 */
function CreateDateColumn(options) {
    return function (object, propertyName) {
        const reflectedType = ColumnTypes_1.ColumnTypes.typeToString(Reflect.getMetadata("design:type", object, propertyName));
        // if column options are not given then create a new empty options
        if (!options)
            options = {};
        // implicitly set a type, because this column's type cannot be anything else except date
        options = Object.assign({ type: ColumnTypes_1.ColumnTypes.DATETIME }, options);
        // create and register a new column metadata
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            propertyType: reflectedType,
            mode: "createDate",
            options: options
        };
        container_1.getMetadataArgsStorage().columns.add(args);
    };
}
exports.CreateDateColumn = CreateDateColumn;

//# sourceMappingURL=CreateDateColumn.js.map
