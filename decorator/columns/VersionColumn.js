"use strict";
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
const container_1 = require("../../container");
/**
 * This column will store a number - version of the entity.
 * Every time your entity will be persisted, this number will be increased by one -
 * so you can organize visioning and update strategies of your entity.
 */
function VersionColumn(options) {
    return function (object, propertyName) {
        const reflectedType = ColumnTypes_1.ColumnTypes.typeToString(Reflect.getMetadata("design:type", object, propertyName));
        // if column options are not given then create a new empty options
        if (!options)
            options = {};
        // implicitly set a type, because this column's type cannot be anything else except date
        options = Object.assign({ type: ColumnTypes_1.ColumnTypes.INTEGER }, options);
        // todo: check if reflectedType is number too
        // create and register a new column metadata
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            propertyType: reflectedType,
            mode: "version",
            options: options
        };
        container_1.getMetadataArgsStorage().columns.add(args);
    };
}
exports.VersionColumn = VersionColumn;

//# sourceMappingURL=VersionColumn.js.map
