"use strict";
const container_1 = require("../container");
/**
 * Decorator registers a new naming strategy to be used in naming things.
 */
function NamingStrategy(name) {
    return function (target) {
        const strategyName = name ? name : target.name;
        const args = {
            target: target,
            name: strategyName
        };
        container_1.getMetadataArgsStorage().namingStrategies.add(args);
    };
}
exports.NamingStrategy = NamingStrategy;

//# sourceMappingURL=NamingStrategy.js.map
