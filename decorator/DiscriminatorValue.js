"use strict";
const container_1 = require("../container");
/**
 * If entity is a child table of some table, it should have a discriminator value.
 * This decorator sets custom discriminator value for the entity.
 */
function DiscriminatorValue(value) {
    return function (target) {
        const args = {
            target: target,
            value: value
        };
        container_1.getMetadataArgsStorage().discriminatorValues.add(args);
    };
}
exports.DiscriminatorValue = DiscriminatorValue;

//# sourceMappingURL=DiscriminatorValue.js.map
