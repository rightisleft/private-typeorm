"use strict";
const container_1 = require("../../container");
const RelationTypes_1 = require("../../metadata/types/RelationTypes");
/**
 * Marks a specific property of the class as a parent of the tree.
 */
function TreeParent(options) {
    return function (object, propertyName) {
        if (!options)
            options = {};
        const reflectedType = Reflect.getMetadata("design:type", object, propertyName);
        const isLazy = reflectedType && typeof reflectedType.name === "string" && reflectedType.name.toLowerCase() === "promise";
        const args = {
            isTreeParent: true,
            target: object.constructor,
            propertyName: propertyName,
            propertyType: reflectedType,
            isLazy: isLazy,
            relationType: RelationTypes_1.RelationTypes.MANY_TO_ONE,
            type: () => object.constructor,
            options: options
        };
        container_1.getMetadataArgsStorage().relations.add(args);
    };
}
exports.TreeParent = TreeParent;

//# sourceMappingURL=TreeParent.js.map
