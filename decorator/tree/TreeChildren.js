"use strict";
const container_1 = require("../../container");
const RelationTypes_1 = require("../../metadata/types/RelationTypes");
/**
 * Marks a specific property of the class as a children of the tree.
 */
function TreeChildren(options) {
    return function (object, propertyName) {
        if (!options)
            options = {};
        const reflectedType = Reflect.getMetadata("design:type", object, propertyName);
        const isLazy = reflectedType && typeof reflectedType.name === "string" && reflectedType.name.toLowerCase() === "promise";
        // add one-to-many relation for this 
        const args = {
            isTreeChildren: true,
            target: object.constructor,
            propertyName: propertyName,
            propertyType: reflectedType,
            isLazy: isLazy,
            relationType: RelationTypes_1.RelationTypes.ONE_TO_MANY,
            type: () => object.constructor,
            options: options
        };
        container_1.getMetadataArgsStorage().relations.add(args);
    };
}
exports.TreeChildren = TreeChildren;

//# sourceMappingURL=TreeChildren.js.map
