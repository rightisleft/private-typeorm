"use strict";
const container_1 = require("../../container");
const ColumnTypes_1 = require("../../metadata/types/ColumnTypes");
/**
 * Creates a "level"/"length" column to the table that holds a closure table.
 */
function TreeLevelColumn() {
    return function (object, propertyName) {
        const reflectedType = ColumnTypes_1.ColumnTypes.typeToString(Reflect.getMetadata("design:type", object, propertyName));
        // implicitly set a type, because this column's type cannot be anything else except number
        const options = { type: ColumnTypes_1.ColumnTypes.INTEGER };
        // create and register a new column metadata
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            propertyType: reflectedType,
            mode: "treeLevel",
            options: options
        };
        container_1.getMetadataArgsStorage().columns.add(args);
    };
}
exports.TreeLevelColumn = TreeLevelColumn;

//# sourceMappingURL=TreeLevelColumn.js.map
