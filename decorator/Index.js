"use strict";
const container_1 = require("../container");
/**
 * Composite index must be set on entity classes and must specify entity's fields to be indexed.
 */
function Index(nameOrFields, maybeFieldsOrOptions, maybeOptions) {
    const name = typeof nameOrFields === "string" ? nameOrFields : undefined;
    const fields = typeof nameOrFields === "string" ? maybeFieldsOrOptions : nameOrFields;
    const options = (typeof maybeFieldsOrOptions === "object" && !Array.isArray(maybeFieldsOrOptions)) ? maybeFieldsOrOptions : maybeOptions;
    return function (clsOrObject, propertyName) {
        const args = {
            target: propertyName ? clsOrObject.constructor : clsOrObject,
            name: name,
            columns: propertyName ? [propertyName] : fields,
            unique: options && options.unique ? true : false
        };
        container_1.getMetadataArgsStorage().indices.add(args);
    };
}
exports.Index = Index;

//# sourceMappingURL=Index.js.map
