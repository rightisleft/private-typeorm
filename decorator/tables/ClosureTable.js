"use strict";
const container_1 = require("../../container");
/**
 * Used on a tables that stores its children in a tree using closure deisgn pattern.
 */
function ClosureTable(name, options) {
    return function (target) {
        const args = {
            target: target,
            name: name,
            type: "closure",
            orderBy: options && options.orderBy ? options.orderBy : undefined,
            skipSchemaSync: !!(options && options.skipSchemaSync === true)
        };
        container_1.getMetadataArgsStorage().tables.add(args);
    };
}
exports.ClosureTable = ClosureTable;

//# sourceMappingURL=ClosureTable.js.map
