"use strict";
const container_1 = require("../../container");
/**
 * Special type of the table used in the single-table inherited tables.
 */
function SingleTableChild() {
    return function (target) {
        const args = {
            target: target,
            name: undefined,
            type: "single-table-child",
            orderBy: undefined
        };
        container_1.getMetadataArgsStorage().tables.add(args);
    };
}
exports.SingleTableChild = SingleTableChild;

//# sourceMappingURL=SingleTableChild.js.map
