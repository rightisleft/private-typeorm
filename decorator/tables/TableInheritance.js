"use strict";
const container_1 = require("../../container");
/**
 * Sets what kind of table-inheritance table will use.
 */
function TableInheritance(type) {
    return function (target) {
        const args = {
            target: target,
            type: type
        };
        container_1.getMetadataArgsStorage().inheritances.add(args);
    };
}
exports.TableInheritance = TableInheritance;

//# sourceMappingURL=TableInheritance.js.map
