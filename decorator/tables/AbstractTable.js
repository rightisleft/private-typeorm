"use strict";
const container_1 = require("../../container");
/**
 * Abstract table is a table that contains columns and relations for all tables that will inherit this table.
 * Database table for the abstract table is not created.
 */
function AbstractTable() {
    return function (target) {
        const args = {
            target: target,
            name: undefined,
            type: "abstract"
        };
        container_1.getMetadataArgsStorage().tables.add(args);
    };
}
exports.AbstractTable = AbstractTable;

//# sourceMappingURL=AbstractTable.js.map
