"use strict";
const container_1 = require("../../container");
/**
 * Special type of the table used in the class-table inherited tables.
 */
function ClassTableChild(tableName, options) {
    return function (target) {
        const args = {
            target: target,
            name: tableName,
            type: "class-table-child",
            orderBy: options && options.orderBy ? options.orderBy : undefined,
            skipSchemaSync: !!(options && options.skipSchemaSync === true)
        };
        container_1.getMetadataArgsStorage().tables.add(args);
    };
}
exports.ClassTableChild = ClassTableChild;

//# sourceMappingURL=ClassTableChild.js.map
