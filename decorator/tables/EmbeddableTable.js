"use strict";
const container_1 = require("../../container");
/**
 * This decorators is used on the entities that must be embedded into the tables.
 */
function EmbeddableTable() {
    return function (target) {
        const args = {
            target: target,
            type: "embeddable",
            orderBy: undefined
        };
        container_1.getMetadataArgsStorage().tables.add(args);
    };
}
exports.EmbeddableTable = EmbeddableTable;

//# sourceMappingURL=EmbeddableTable.js.map
