"use strict";
const container_1 = require("../../container");
/**
 * Special decorator used to extract relation id into separate entity property.
 */
function RelationId(relation) {
    return function (object, propertyName) {
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            relation: relation
        };
        container_1.getMetadataArgsStorage().relationIds.add(args);
    };
}
exports.RelationId = RelationId;

//# sourceMappingURL=RelationId.js.map
