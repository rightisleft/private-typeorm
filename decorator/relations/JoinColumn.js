"use strict";
const container_1 = require("../../container");
/**
 * JoinColumn decorator used on one-to-one relations to specify owner side of relationship.
 * It also can be used on both one-to-one and many-to-one relations to specify custom column name
 * or custom referenced column.
 */
function JoinColumn(options) {
    return function (object, propertyName) {
        options = options || {};
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            name: options.name,
            referencedColumnName: options.referencedColumnName
        };
        container_1.getMetadataArgsStorage().joinColumns.add(args);
    };
}
exports.JoinColumn = JoinColumn;

//# sourceMappingURL=JoinColumn.js.map
