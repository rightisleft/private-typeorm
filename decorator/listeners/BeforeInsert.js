"use strict";
const container_1 = require("../../container");
const EventListenerTypes_1 = require("../../metadata/types/EventListenerTypes");
/**
 * Calls a method on which this decorator is applied before this entity insertion.
 */
function BeforeInsert() {
    return function (object, propertyName) {
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            type: EventListenerTypes_1.EventListenerTypes.BEFORE_INSERT
        };
        container_1.getMetadataArgsStorage().entityListeners.add(args);
    };
}
exports.BeforeInsert = BeforeInsert;

//# sourceMappingURL=BeforeInsert.js.map
