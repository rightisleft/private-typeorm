"use strict";
const container_1 = require("../../container");
const EventListenerTypes_1 = require("../../metadata/types/EventListenerTypes");
/**
 * Calls a method on which this decorator is applied after this entity removal.
 */
function AfterRemove() {
    return function (object, propertyName) {
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            type: EventListenerTypes_1.EventListenerTypes.AFTER_REMOVE
        };
        container_1.getMetadataArgsStorage().entityListeners.add(args);
    };
}
exports.AfterRemove = AfterRemove;

//# sourceMappingURL=AfterRemove.js.map
