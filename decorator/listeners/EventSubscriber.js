"use strict";
const container_1 = require("../../container");
/**
 * Classes decorated with this decorator will listen to ORM events and their methods will be triggered when event
 * occurs. Those classes must implement EventSubscriberInterface interface.
 */
function EventSubscriber() {
    return function (target) {
        const args = {
            target: target
        };
        container_1.getMetadataArgsStorage().entitySubscribers.add(args);
    };
}
exports.EventSubscriber = EventSubscriber;

//# sourceMappingURL=EventSubscriber.js.map
