"use strict";
const container_1 = require("../../container");
const EventListenerTypes_1 = require("../../metadata/types/EventListenerTypes");
/**
 * Calls a method on which this decorator is applied after this entity update.
 */
function AfterUpdate() {
    return function (object, propertyName) {
        const args = {
            target: object.constructor,
            propertyName: propertyName,
            type: EventListenerTypes_1.EventListenerTypes.AFTER_UPDATE
        };
        container_1.getMetadataArgsStorage().entityListeners.add(args);
    };
}
exports.AfterUpdate = AfterUpdate;

//# sourceMappingURL=AfterUpdate.js.map
