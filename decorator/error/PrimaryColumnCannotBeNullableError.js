"use strict";
class PrimaryColumnCannotBeNullableError extends Error {
    constructor(object, propertyName) {
        super();
        this.name = "PrimaryColumnCannotBeNullableError";
        this.message = `Primary column ${object.constructor.name}#${propertyName} cannot be nullable. ` +
            `Its not allowed for primary keys. Try to remove nullable option.`;
    }
}
exports.PrimaryColumnCannotBeNullableError = PrimaryColumnCannotBeNullableError;

//# sourceMappingURL=PrimaryColumnCannotBeNullableError.js.map
