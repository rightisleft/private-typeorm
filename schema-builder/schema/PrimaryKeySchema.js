"use strict";
/**
 * Primary key from the database stored in this class.
 */
class PrimaryKeySchema {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(name, columnName) {
        this.name = name;
        this.columnName = columnName;
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Creates a new copy of this primary key with exactly same properties.
     */
    clone() {
        return new PrimaryKeySchema(this.name, this.columnName);
    }
}
exports.PrimaryKeySchema = PrimaryKeySchema;

//# sourceMappingURL=PrimaryKeySchema.js.map
