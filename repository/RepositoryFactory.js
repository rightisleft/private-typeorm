"use strict";
const TreeRepository_1 = require("./TreeRepository");
const Repository_1 = require("./Repository");
const SpecificRepository_1 = require("./SpecificRepository");
/**
 * Factory used to create different types of repositories.
 */
class RepositoryFactory {
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Creates a regular repository.
     */
    createRepository(connection, metadata, queryRunnerProvider) {
        return new Repository_1.Repository(connection, metadata, queryRunnerProvider);
    }
    /**
     * Creates a tree repository.
     */
    createTreeRepository(connection, metadata, queryRunnerProvider) {
        return new TreeRepository_1.TreeRepository(connection, metadata, queryRunnerProvider);
    }
    /**
     * Creates a specific repository.
     */
    createSpecificRepository(connection, metadata, repository, queryRunnerProvider) {
        return new SpecificRepository_1.SpecificRepository(connection, metadata, repository, queryRunnerProvider);
    }
}
exports.RepositoryFactory = RepositoryFactory;

//# sourceMappingURL=RepositoryFactory.js.map
