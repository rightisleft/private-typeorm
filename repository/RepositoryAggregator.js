"use strict";
const index_1 = require("../index");
const RepositoryFactory_1 = require("./RepositoryFactory");
/**
 * Aggregates all repositories of the specific metadata.
 */
class RepositoryAggregator {
    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------
    constructor(connection, metadata, queryRunnerProvider) {
        const repositoryFactory = index_1.getFromContainer(RepositoryFactory_1.RepositoryFactory);
        this.metadata = metadata;
        if (metadata.table.isClosure) {
            this.repository = this.treeRepository = repositoryFactory.createTreeRepository(connection, metadata, queryRunnerProvider);
        }
        else {
            this.repository = repositoryFactory.createRepository(connection, metadata, queryRunnerProvider);
        }
        this.specificRepository = repositoryFactory.createSpecificRepository(connection, metadata, this.repository, queryRunnerProvider);
    }
}
exports.RepositoryAggregator = RepositoryAggregator;

//# sourceMappingURL=RepositoryAggregator.js.map
