"use strict";
/**
 */
class Alias {
    constructor(name) {
        this.name = name;
    }
    get selection() {
        return this.parentAliasName + "." + this.parentPropertyName;
    }
}
exports.Alias = Alias;

//# sourceMappingURL=Alias.js.map
