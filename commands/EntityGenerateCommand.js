"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
/**
 * Generates a new entity.
 */
class EntityGenerateCommand {
    constructor() {
        this.command = "entity:generate";
        this.describe = "Generates a new entity.";
    }
    builder(yargs) {
        return yargs
            .option("c", {
            alias: "connection",
            default: "default",
            describe: "Name of the connection on which to run a query"
        });
    }
    handler(argv) {
        return __awaiter(this, void 0, void 0, function* () {
            // todo.
        });
    }
}
exports.EntityGenerateCommand = EntityGenerateCommand;

//# sourceMappingURL=EntityGenerateCommand.js.map
