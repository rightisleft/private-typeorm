/**
 * Generates a new entity.
 */
export declare class EntityGenerateCommand {
    command: string;
    describe: string;
    builder(yargs: any): any;
    handler(argv: any): Promise<void>;
}
