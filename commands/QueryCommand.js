"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const index_1 = require("../index");
/**
 * Executes an sql query on the given connection.
 */
class QueryCommand {
    constructor() {
        this.command = "query";
        this.describe = "Executes given SQL query on a default connection. Specify connection name to run query on a specific connection.";
    }
    builder(yargs) {
        return yargs
            .option("c", {
            alias: "connection",
            default: "default",
            describe: "Name of the connection on which to run a query"
        });
    }
    handler(argv) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                process.env.SKIP_SCHEMA_CREATION = true;
                const connectionName = "default" || argv.connection;
                const connection = yield index_1.createConnection(connectionName);
                const queryRunner = yield connection.driver.createQueryRunner();
                const queryResult = yield queryRunner.query(argv._[1]);
                console.log("Query executed. Result: ", queryResult);
                yield queryRunner.release();
                yield connection.close();
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
}
exports.QueryCommand = QueryCommand;

//# sourceMappingURL=QueryCommand.js.map
