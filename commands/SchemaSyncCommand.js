"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const index_1 = require("../index");
/**
 * Synchronizes database schema with entities.
 */
class SchemaSyncCommand {
    constructor() {
        this.command = "schema:sync";
        this.describe = "Synchronizes your entities with database schema. It runs schema update queries on all connections you have. " +
            "To run update queries on a concrete connection use -c option.";
    }
    builder(yargs) {
        return yargs.option("c", {
            alias: "connection",
            default: "default",
            describe: "Name of the connection on which schema synchronization needs to to run"
        });
    }
    handler(argv) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                process.env.LOGGER_CLI_SCHEMA_SYNC = true;
                process.env.SKIP_SCHEMA_CREATION = true;
                if (argv.connection) {
                    const connection = yield index_1.createConnection(argv.connection);
                    yield connection.syncSchema(false);
                    yield connection.close();
                }
                else {
                    const connections = yield index_1.createConnections();
                    yield Promise.all(connections.map(connection => connection.syncSchema(false)));
                    yield Promise.all(connections.map(connection => connection.close()));
                }
            }
            catch (err) {
                console.log(err);
                throw err;
            }
        });
    }
}
exports.SchemaSyncCommand = SchemaSyncCommand;

//# sourceMappingURL=SchemaSyncCommand.js.map
