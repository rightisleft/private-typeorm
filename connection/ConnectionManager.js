"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const fs = require("fs");
const Connection_1 = require("./Connection");
const ConnectionNotFoundError_1 = require("./error/ConnectionNotFoundError");
const MysqlDriver_1 = require("../driver/mysql/MysqlDriver");
const MissingDriverError_1 = require("./error/MissingDriverError");
const PostgresDriver_1 = require("../driver/postgres/PostgresDriver");
const AlreadyHasActiveConnectionError_1 = require("./error/AlreadyHasActiveConnectionError");
const Logger_1 = require("../logger/Logger");
const SqliteDriver_1 = require("../driver/sqlite/SqliteDriver");
const OracleDriver_1 = require("../driver/oracle/OracleDriver");
const SqlServerDriver_1 = require("../driver/sqlserver/SqlServerDriver");
const OrmUtils_1 = require("../util/OrmUtils");
const CannotDetermineConnectionOptionsError_1 = require("./error/CannotDetermineConnectionOptionsError");
/**
 * ConnectionManager is used to store and manage all these different connections.
 * It also provides useful factory methods to simplify connection creation.
 */
class ConnectionManager {
    constructor() {
        // -------------------------------------------------------------------------
        // Protected Properties
        // -------------------------------------------------------------------------
        /**
         * List of connections registered in this connection manager.
         */
        this.connections = [];
    }
    // -------------------------------------------------------------------------
    // Public Methods
    // -------------------------------------------------------------------------
    /**
     * Checks if connection with the given name exist in the manager.
     */
    has(name) {
        return !!this.connections.find(connection => connection.name === name);
    }
    /**
     * Gets registered connection with the given name.
     * If connection name is not given then it will get a default connection.
     * Throws exception if connection with the given name was not found.
     */
    get(name = "default") {
        const connection = this.connections.find(connection => connection.name === name);
        if (!connection)
            throw new ConnectionNotFoundError_1.ConnectionNotFoundError(name);
        return connection;
    }
    /**
     * Creates a new connection based on the given connection options and registers it in the manager.
     * You need to manually call #connect method to establish connection.
     * Note that dropSchemaOnConnection and autoSchemaSync options of a ConnectionOptions will not work there - use
     * createAndConnect method to use them.
     */
    create(options) {
        const logger = new Logger_1.Logger(options.logging || {});
        const driver = this.createDriver(options.driver, logger);
        const connection = this.createConnection(options.name || "default", driver, logger);
        // import entity schemas
        if (options.entitySchemas) {
            const [directories, classes] = this.splitStringsAndClasses(options.entitySchemas);
            connection
                .importEntitySchemas(classes)
                .importEntitySchemaFromDirectories(directories);
        }
        // import entities
        if (options.entities) {
            const [directories, classes] = this.splitStringsAndClasses(options.entities);
            connection
                .importEntities(classes)
                .importEntitiesFromDirectories(directories);
        }
        // import subscriber
        if (options.subscribers) {
            const [directories, classes] = this.splitStringsAndClasses(options.subscribers);
            connection
                .importSubscribers(classes)
                .importSubscribersFromDirectories(directories);
        }
        // import naming strategies
        if (options.namingStrategies) {
            const [directories, classes] = this.splitStringsAndClasses(options.namingStrategies);
            connection
                .importNamingStrategies(classes)
                .importNamingStrategiesFromDirectories(directories);
        }
        // set naming strategy to be used for this connection
        if (options.usedNamingStrategy)
            connection.useNamingStrategy(options.usedNamingStrategy);
        return connection;
    }
    /**
     * Creates connection and and registers it in the manager.
     */
    createAndConnect(optionsOrConnectionNameFromConfig, ormConfigPath) {
        return __awaiter(this, void 0, void 0, function* () {
            // if connection options are given, then create connection from them
            if (optionsOrConnectionNameFromConfig && optionsOrConnectionNameFromConfig instanceof Object)
                return this.createAndConnectByConnectionOptions(optionsOrConnectionNameFromConfig);
            // if connection name is specified then explicitly try to load connection options from it
            if (typeof optionsOrConnectionNameFromConfig === "string")
                return this.createFromConfigAndConnect(optionsOrConnectionNameFromConfig || "default", ormConfigPath);
            // if nothing is specified then try to silently load config from ormconfig.json
            if (this.hasDefaultConfigurationInConfigurationFile())
                return this.createFromConfigAndConnect("default");
            // if driver type is set in environment variables then try to create connection from env variables
            if (this.hasDefaultConfigurationInEnvironmentVariables())
                return this.createFromEnvAndConnect();
            throw new CannotDetermineConnectionOptionsError_1.CannotDetermineConnectionOptionsError();
        });
    }
    /**
     * Creates connections and and registers them in the manager.
     */
    createAndConnectToAll(optionsOrOrmConfigFilePath) {
        return __awaiter(this, void 0, void 0, function* () {
            // if connection options are given, then create connection from them
            if (optionsOrOrmConfigFilePath && optionsOrOrmConfigFilePath instanceof Array)
                return Promise.all(optionsOrOrmConfigFilePath.map(options => {
                    return this.createAndConnectByConnectionOptions(options);
                }));
            // if connection name is specified then explicitly try to load connection options from it
            if (typeof optionsOrOrmConfigFilePath === "string")
                return this.createFromConfigAndConnectToAll(optionsOrOrmConfigFilePath);
            // if nothing is specified then try to silently load config from ormconfig.json
            if (this.hasOrmConfigurationFile())
                return this.createFromConfigAndConnectToAll();
            // if driver type is set in environment variables then try to create connection from env variables
            if (this.hasDefaultConfigurationInEnvironmentVariables())
                return [yield this.createFromEnvAndConnect()];
            throw new CannotDetermineConnectionOptionsError_1.CannotDetermineConnectionOptionsError();
        });
    }
    // -------------------------------------------------------------------------
    // Protected Methods
    // -------------------------------------------------------------------------
    /**
     * Checks if ormconfig.json exists.
     */
    hasOrmConfigurationFile() {
        const path = require("app-root-path").path + "/ormconfig.json";
        if (!fs.existsSync(path))
            return false;
        const configuration = require(path);
        if (configuration instanceof Array) {
            return configuration
                .filter(options => !options.environment || options.environment === process.env.NODE_ENV)
                .length > 0;
        }
        else if (configuration instanceof Object) {
            if (configuration.environment && configuration.environment !== process.env.NODE_ENV)
                return false;
            return Object.keys(configuration).length > 0;
        }
        return false;
    }
    /**
     * Checks if there is a default connection in the ormconfig.json file.
     */
    hasDefaultConfigurationInConfigurationFile() {
        const path = require("app-root-path").path + "/ormconfig.json";
        if (!fs.existsSync(path))
            return false;
        const configuration = require(path);
        if (configuration instanceof Array) {
            return !!configuration
                .filter(options => !options.environment || options.environment === process.env.NODE_ENV)
                .find(config => !!config.name || config.name === "default");
        }
        else if (configuration instanceof Object) {
            if (!configuration.name ||
                configuration.name !== "default")
                return false;
            if (configuration.environment && configuration.environment !== process.env.NODE_ENV)
                return false;
            return true;
        }
        return false;
    }
    /**
     * Checks if environment variables contains connection options.
     */
    hasDefaultConfigurationInEnvironmentVariables() {
        return !!process.env.TYPEORM_DRIVER_TYPE;
    }
    /**
     * Allows to quickly create a connection based on the environment variable values.
     */
    createFromEnvAndConnect() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.createAndConnectByConnectionOptions({
                driver: {
                    type: process.env.TYPEORM_DRIVER_TYPE,
                    url: process.env.TYPEORM_URL,
                    host: process.env.TYPEORM_HOST,
                    port: process.env.TYPEORM_PORT,
                    username: process.env.TYPEORM_USERNAME,
                    password: process.env.TYPEORM_PASSWORD,
                    database: process.env.TYPEORM_DATABASE,
                    sid: process.env.TYPEORM_SID,
                    storage: process.env.TYPEORM_STORAGE,
                    usePool: process.env.TYPEORM_USE_POOL !== undefined ? OrmUtils_1.OrmUtils.toBoolean(process.env.TYPEORM_USE_POOL) : undefined,
                    extra: process.env.TYPEORM_DRIVER_EXTRA ? JSON.parse(process.env.TYPEORM_DRIVER_EXTRA) : undefined
                },
                autoSchemaSync: OrmUtils_1.OrmUtils.toBoolean(process.env.TYPEORM_AUTO_SCHEMA_SYNC),
                entities: process.env.TYPEORM_ENTITIES ? process.env.TYPEORM_ENTITIES.split(",") : [],
                subscribers: process.env.TYPEORM_SUBSCRIBERS ? process.env.TYPEORM_SUBSCRIBERS.split(",") : [],
                entitySchemas: process.env.TYPEORM_ENTITY_SCHEMAS ? process.env.TYPEORM_ENTITY_SCHEMAS.split(",") : [],
                namingStrategies: process.env.TYPEORM_NAMING_STRATEGIES ? process.env.TYPEORM_NAMING_STRATEGIES.split(",") : [],
                usedNamingStrategy: process.env.TYPEORM_USED_NAMING_STRATEGY,
                logging: {
                    logQueries: OrmUtils_1.OrmUtils.toBoolean(process.env.TYPEORM_LOGGING_QUERIES),
                    logFailedQueryError: OrmUtils_1.OrmUtils.toBoolean(process.env.TYPEORM_LOGGING_FAILED_QUERIES),
                    logOnlyFailedQueries: OrmUtils_1.OrmUtils.toBoolean(process.env.TYPEORM_LOGGING_ONLY_FAILED_QUERIES),
                }
            });
        });
    }
    /**
     * Creates a new connection based on the connection options from "ormconfig.json"
     * and registers a new connection in the manager.
     * Optionally you can specify a path to the json configuration.
     * If path is not given, then ormconfig.json file will be searched near node_modules directory.
     */
    createFromConfigAndConnectToAll(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const optionsArray = require(path || (require("app-root-path").path + "/ormconfig.json"));
            if (!optionsArray)
                throw new Error(`Configuration ${path || "ormconfig.json"} was not found. Add connection configuration inside ormconfig.json file.`);
            const promises = optionsArray
                .filter(options => !options.environment || options.environment === process.env.NODE_ENV) // skip connection creation if environment is set in the options, and its not equal to the value in the NODE_ENV variable
                .map(options => this.createAndConnectByConnectionOptions(options));
            return Promise.all(promises);
        });
    }
    /**
     * Creates a new connection based on the connection options from "ormconfig.json"
     * and registers a new connection in the manager.
     * Optionally you can specify a path to the json configuration.
     * If path is not given, then ormconfig.json file will be searched near node_modules directory.
     */
    createFromConfigAndConnect(connectionName, path) {
        return __awaiter(this, void 0, void 0, function* () {
            const optionsArray = require(path || (require("app-root-path").path + "/ormconfig.json"));
            if (!optionsArray)
                throw new Error(`Configuration ${path || "ormconfig.json"} was not found. Add connection configuration inside ormconfig.json file.`);
            const environmentLessOptions = optionsArray.filter(options => (options.name || "default") === connectionName);
            const options = environmentLessOptions.filter(options => !options.environment || options.environment === process.env.NODE_ENV); // skip connection creation if environment is set in the options, and its not equal to the value in the NODE_ENV variable
            if (!options.length)
                throw new Error(`Connection "${connectionName}" ${process.env.NODE_ENV ? "for the environment " + process.env.NODE_ENV + " " : ""}was not found in the json configuration file.` +
                    (environmentLessOptions.length ? ` However there are such configurations for other environments: ${environmentLessOptions.map(options => options.environment).join(", ")}.` : ""));
            return this.createAndConnectByConnectionOptions(options[0]);
        });
    }
    /**
     * Creates a new connection based on the given connection options and registers a new connection in the manager.
     */
    createAndConnectByConnectionOptions(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = this.create(options);
            // connect to the database
            yield connection.connect();
            // if option is set - drop schema once connection is done
            if (options.dropSchemaOnConnection && !process.env.SKIP_SCHEMA_CREATION)
                yield connection.dropDatabase();
            // if option is set - automatically synchronize a schema
            if (options.autoSchemaSync && !process.env.SKIP_SCHEMA_CREATION)
                yield connection.syncSchema();
            return connection;
        });
    }
    /**
     * Splits given array of mixed strings and / or functions into two separate array of string and array of functions.
     */
    splitStringsAndClasses(strAndClses) {
        return [
            strAndClses.filter(str => typeof str === "string"),
            strAndClses.filter(cls => typeof cls !== "string"),
        ];
    }
    /**
     * Creates a new driver based on the given driver type and options.
     */
    createDriver(options, logger) {
        switch (options.type) {
            case "mysql":
                return new MysqlDriver_1.MysqlDriver(options, logger, undefined, "mysql");
            case "mysql2":
                return new MysqlDriver_1.MysqlDriver(options, logger, undefined, "mysql2");
            case "postgres":
                return new PostgresDriver_1.PostgresDriver(options, logger);
            case "mariadb":
                return new MysqlDriver_1.MysqlDriver(options, logger);
            case "sqlite":
                return new SqliteDriver_1.SqliteDriver(options, logger);
            case "oracle":
                return new OracleDriver_1.OracleDriver(options, logger);
            case "mssql":
                return new SqlServerDriver_1.SqlServerDriver(options, logger);
            default:
                throw new MissingDriverError_1.MissingDriverError(options.type);
        }
    }
    /**
     * Creates a new connection and registers it in the connection manager.
     */
    createConnection(name, driver, logger) {
        const existConnection = this.connections.find(connection => connection.name === name);
        if (existConnection) {
            if (existConnection.isConnected)
                throw new AlreadyHasActiveConnectionError_1.AlreadyHasActiveConnectionError(name);
            this.connections.splice(this.connections.indexOf(existConnection), 1);
        }
        const connection = new Connection_1.Connection(name, driver, logger);
        this.connections.push(connection);
        return connection;
    }
}
exports.ConnectionManager = ConnectionManager;

//# sourceMappingURL=ConnectionManager.js.map
