"use strict";
/**
 * Thrown when consumer tries to import entities / entity schemas / subscribers / naming strategies after connection
 * is established.
 */
class CannotImportAlreadyConnectedError extends Error {
    constructor(importStuff, connectionName) {
        super();
        this.name = "CannotImportAlreadyConnected";
        this.message = `Cannot import ${importStuff} for "${connectionName}" connection because connection to the database already established.`;
        this.stack = new Error().stack;
    }
}
exports.CannotImportAlreadyConnectedError = CannotImportAlreadyConnectedError;

//# sourceMappingURL=CannotImportAlreadyConnectedError.js.map
