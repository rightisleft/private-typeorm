"use strict";
/**
 * Thrown when consumer tries to get connection that does not exist.
 */
class ConnectionNotFoundError extends Error {
    constructor(name) {
        super();
        this.name = "ConnectionNotFoundError";
        this.message = `Connection "${name}" was not found.`;
        this.stack = new Error().stack;
    }
}
exports.ConnectionNotFoundError = ConnectionNotFoundError;

//# sourceMappingURL=ConnectionNotFoundError.js.map
