"use strict";
const EntityMetadata_1 = require("../metadata/EntityMetadata");
const ColumnMetadata_1 = require("../metadata/ColumnMetadata");
const ForeignKeyMetadata_1 = require("../metadata/ForeignKeyMetadata");
const TableMetadata_1 = require("../metadata/TableMetadata");
const ColumnTypes_1 = require("../metadata/types/ColumnTypes");
/**
 * Helps to create EntityMetadatas for junction tables for closure tables.
 */
class ClosureJunctionEntityMetadataBuilder {
    build(driver, lazyRelationsWrapper, args) {
        const columns = [
            new ColumnMetadata_1.ColumnMetadata({
                target: "__virtual__",
                propertyName: "__virtual__",
                propertyType: args.primaryColumn.type,
                mode: "virtual",
                options: {
                    length: args.primaryColumn.length,
                    type: args.primaryColumn.type,
                    name: "ancestor"
                }
            }),
            new ColumnMetadata_1.ColumnMetadata({
                target: "__virtual__",
                propertyName: "__virtual__",
                propertyType: args.primaryColumn.type,
                mode: "virtual",
                options: {
                    length: args.primaryColumn.length,
                    type: args.primaryColumn.type,
                    name: "descendant"
                }
            })
        ];
        if (args.hasTreeLevelColumn) {
            columns.push(new ColumnMetadata_1.ColumnMetadata({
                target: "__virtual__",
                propertyName: "__virtual__",
                propertyType: ColumnTypes_1.ColumnTypes.INTEGER,
                mode: "virtual",
                options: {
                    type: ColumnTypes_1.ColumnTypes.INTEGER,
                    name: "level"
                }
            }));
        }
        const closureJunctionTableMetadata = new TableMetadata_1.TableMetadata({
            target: "__virtual__",
            name: args.table.name,
            type: "closure-junction"
        });
        return new EntityMetadata_1.EntityMetadata({
            target: "__virtual__",
            tablesPrefix: driver.options.tablesPrefix,
            namingStrategy: args.namingStrategy,
            tableMetadata: closureJunctionTableMetadata,
            columnMetadatas: columns,
            foreignKeyMetadatas: [
                new ForeignKeyMetadata_1.ForeignKeyMetadata([columns[0]], args.table, [args.primaryColumn]),
                new ForeignKeyMetadata_1.ForeignKeyMetadata([columns[1]], args.table, [args.primaryColumn])
            ]
        }, lazyRelationsWrapper);
    }
}
exports.ClosureJunctionEntityMetadataBuilder = ClosureJunctionEntityMetadataBuilder;

//# sourceMappingURL=ClosureJunctionEntityMetadataBuilder.js.map
